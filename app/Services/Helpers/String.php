<?php

namespace App\Services\Helpers;


use Carbon\Carbon;
use Rollbar;
use Unirest;

class String
{
    
    public $return_data;

    /**
     * Beräkna tidsskillnad
     *
     * @param $start
     * @param bool $end
     * @internal param $sec
     * @return string
     */
    public static function print_time($start = false, $end)
    {

        if(!$start) {
            $date = new \DateTime('now', new \DateTimeZone('Europe/Stockholm'));
            $start = $date->getTimestamp();
        }

        $difference = $end-$start;

        if($difference < 59) {
            return $difference . ' ' . trans('general.seconds');
        } elseif($difference < 3600) {
            $val = round($difference/60);

            if($val == 1) {
                return $val . ' ' . trans('general.minute');
            } else {
                return $val . ' ' . trans('general.minutes');
            }

        } elseif($difference < 86400) {

            $val = round($difference/3600);

            if($val == 1) {
                return $val . ' ' . trans('general.hour');
            } else {
                return $val . ' ' . trans('general.hours');
            }

        } else {
            $val = round($difference/86400);

            if($val == 1) {
                return $val . ' ' . trans('general.day');
            } else {
                return $val . ' ' . trans('general.days');
            }
        }

    }

    /**
     * Converts an abbreviation of an addon to readable format.
     *
     * @param $abbreviation
     * @return string
     */
    public static function addonAbbreviationToText($abbreviation)
    {

        if($abbreviation == "proxy") {
            $text = trans('general.proxy');
        } elseif($abbreviation == "filtering") {
            $text = trans('general.filtering');
        } elseif($abbreviation == "public-ipv4") {
            $text = trans('general.public_ipv4');
        } elseif($abbreviation == "multihop") {
            $text = trans('general.multihop');
        } else {
            $text = trans('general.no_addon');
        }

        return $text;
    }

    /**
     * Removes duplicate values in an array
     *
     * @param $array
     * @param $field
     * @return array
     */
    public static function removeDuplicate($array, $field)
    {
        $cmp = [];
        $new = [];
        foreach ($array as $key => $sub) {
            if(!isset($sub[$field])) {
                $sub[$field] = '00:00:00:00:00:00';
            }
            if(!in_array($sub[$field], $cmp)) {
                $cmp[] = $sub[$field];
            } else {
                unset($array[$key]);
            }
        }

        foreach ($array as $entry) {
            $new[] = $entry;
        }
        return $new;
    }

    public static function parseTimestamp($timestamp)
    {
        if(is_null($timestamp)) {
            return '-';
        }

        return self::print_time(false, $timestamp);
    }

    /**
     * Parses the output of a process in order to display the time it has been running
     *
     * @param $uptime
     * @return string
     */
    public static function parseProcessUptime($uptime) {

        $explode = explode("-", $uptime);
        $retVal = array();

        if(count($explode) == 2) {

            $days = trim($explode[0]);

            if($days > 1) {
                $retVal['days'] = $days . ' ' . trans('general.days');
            } else {
                $retVal['days'] = $days . ' ' . trans('general.day');
            }

            $rest = trim($explode[1]);
        } else {
            $rest = trim($explode[0]);
        }

        $format = explode(":", $rest);

        if($format[0] > 1 || $format[0] == 0) {
            $retVal['hours'] = round($format[0]) . ' ' . trans('general.hours');
        } else {
            $retVal['hours'] = round($format[0]) . ' ' . trans('general.hour');
        }

        if($format[1] > 1 || $format[1] == 0)  {
            $retVal['minutes'] = round($format[1]) . ' ' . trans('general.minutes');
        } else {
            $retVal['minutes'] = round($format[1]) . ' ' . trans('general.minute');
        }

        if($format[2] > 1 || $format[2] == 0) {
            $retVal['seconds'] = round($format[2]) . ' ' . trans('general.seconds');
        } else {
            $retVal['seconds'] = round($format[2]) . ' ' . trans('general.second');
        }

        return implode(', ', $retVal);
    }

    public static function fixHostname($name)
    {
        return str_replace(['å', 'ä', 'ö', ' ' ], ['a', 'a', 'o', '-'], strtolower($name));
    }

    public static function adapterToId($adapter)
    {
        $data = [
            'ovpnse1' => 0,
            'ovpnse2' => 1,
            'ovpnse3' => 2,
            'ovpnse4' => 3,
            'ovpnse5' => 4,
            'ovpnse6' => 5,
        ];

        if(isset($data[$adapter])) {
            return $data[$adapter];
        }

        return null;
    }
}

?>