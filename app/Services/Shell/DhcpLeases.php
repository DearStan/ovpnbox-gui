<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 16-03-10
 * Time: 14:34
 */

namespace App\Services\Shell;


use App\Services\Helpers\String;

class DhcpLeases
{

    /**
     * Fetch all DHCP leases
     *
     * @return array|bool
     */
    public static function get()
    {

        $awk = "/usr/bin/awk";
        /* this pattern sticks comments into a single array item */
        $cleanpattern = "'{ gsub(\"#.*\", \"\");} { gsub(\";\", \"\"); print;}'";
        /* We then split the leases file by } */
        $splitpattern = "'BEGIN { RS=\"}\";} {for (i=1; i<=NF; i++) printf \"%s \", \$i; printf \"}\\n\";}'";

        /* stuff the leases file in a proper format into a array by line */
        exec("/bin/cat /var/dhcpd/var/db/dhcpd.leases | {$awk} {$cleanpattern} | {$awk} {$splitpattern}", $leases_content);
        $leases_count = count($leases_content);
        exec("/usr/sbin/arp -an", $rawdata);

        $arpdata_ip = array();
        $arpdata_mac = array();
        foreach ($rawdata as $line) {
            $elements = explode(' ',$line);
            if ($elements[3] != "(incomplete)") {
                $arpent = array();
                $arpdata_ip[] = trim(str_replace(array('(',')'),'',$elements[1]));
                $arpdata_mac[] = strtolower(trim($elements[3]));
            }
        }
        unset($rawdata);
        $pools = array();
        $leases = array();
        $i = 0;
        $l = 0;
        $p = 0;


        // Put everything together again
        foreach($leases_content as $lease) {

            /* split the line by space */
            $data = explode(" ", $lease);
            /* walk the fields */
            $f = 0;
            $fcount = count($data);
            /* with less than 20 fields there is nothing useful */
            if($fcount < 20) {
                $i++;
                continue;
            }
            while($f < $fcount) {
                switch($data[$f]) {
                    case "failover":
                        $pools[$p]['name'] = trim($data[$f+2], '"');
                        $pools[$p]['mystate'] = $data[$f+7];
                        $pools[$p]['peerstate'] = $data[$f+14];
                        $pools[$p]['mydate'] = $data[$f+10];
                        $pools[$p]['mydate'] .= " " . $data[$f+11];
                        $pools[$p]['peerdate'] = $data[$f+17];
                        $pools[$p]['peerdate'] .= " " . $data[$f+18];
                        $p++;
                        $i++;
                        continue 3;
                    case "lease":
                        $leases[$l]['ip'] = $data[$f+1];
                        $leases[$l]['type'] = "dynamic";
                        $f = $f+2;
                        break;
                    case "starts":
                        $leases[$l]['start'] = $data[$f+2];
                        $leases[$l]['start'] .= " " . $data[$f+3];
                        $f = $f+3;
                        break;
                    case "ends":
                        if ($data[$f+1] == "never") {
                            // Quote from dhcpd.leases(5) man page:
                            // If a lease will never expire, date is never instead of an actual date.
                            $leases[$l]['end'] = gettext("Never");
                            $f = $f+1;
                        } else {
                            $leases[$l]['end'] = $data[$f+2];
                            $leases[$l]['end'] .= " " . $data[$f+3];
                            $f = $f+3;
                        }
                        break;
                    case "tstp":
                        $f = $f+3;
                        break;
                    case "tsfp":
                        $f = $f+3;
                        break;
                    case "atsfp":
                        $f = $f+3;
                        break;
                    case "cltt":
                        $f = $f+3;
                        break;
                    case "binding":
                        switch($data[$f+2]) {
                            case "active":
                                $leases[$l]['act'] = "active";
                                break;
                            case "free":
                                $leases[$l]['act'] = "expired";
                                $leases[$l]['online'] = "offline";
                                break;
                            case "backup":
                                $leases[$l]['act'] = "reserved";
                                $leases[$l]['online'] = "offline";
                                break;
                        }
                        $f = $f+1;
                        break;
                    case "next":
                        /* skip the next binding statement */
                        $f = $f+3;
                        break;
                    case "rewind":
                        /* skip the rewind binding statement */
                        $f = $f+3;
                        break;
                    case "hardware":
                        $leases[$l]['mac'] = $data[$f+2];
                        /* check if it's online and the lease is active */
                        if (in_array($leases[$l]['ip'], $arpdata_ip)) {
                            $leases[$l]['online'] = 'online';
                        } else {
                            $leases[$l]['online'] = 'offline';
                        }
                        $f = $f+2;
                        break;
                    case "client-hostname":
                        if($data[$f+1] <> "") {
                            $leases[$l]['hostname'] = preg_replace('/"/','',$data[$f+1]);
                        } else {
                            $hostname = gethostbyaddr($leases[$l]['ip']);
                            if($hostname <> "") {
                                $leases[$l]['hostname'] = $hostname;
                            }
                        }
                        $f = $f+1;
                        break;
                    case "uid":
                        $f = $f+1;
                        break;
                }
                $f++;
            }
            $l++;
            $i++;
            /* slowly chisel away at the source array */
            array_shift($leases_content);
        }

        foreach($leases as $key => $row) {
            if(isset($row['act']) && ($row['act'] == 'expired')) {
                unset($leases[$key]);
            }

            if(!isset($row['mac'])) {
                $row['mac'] = '00:00:00:00:00:00';
            }
        }

        /* remove duplicate items by mac address */
        if(count($leases) > 0) {
            $leases = String::removeDuplicate($leases, "mac");
        } else {
            return false;
        }

        // Sort the array based on IP
        foreach ($leases as $key => $row) {
            $explode       = explode(".", $row['ip']);
            $ipData[$key]  = $explode[count($explode)-1];
        }

        array_multisort($ipData, SORT_ASC, $leases);

        return $leases;
    }
}