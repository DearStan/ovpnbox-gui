<?php
/**
 * Created by PhpStorm.
 * User: davidwibergh
 * Date: 15-04-19
 * Time: 15:40
 */

namespace App\Services\Shell;


use App\Interfaces;
use App\Services\Ovpn\Api;

class Traceroute {

    public function get($ip, $ping = false)
    {

        // Create and execute bash script that returns the amount of hops
       // $query = '/usr/sbin/traceroute -i ' . escapeshellarg(Interfaces::where('type', 'wan')->first()->name) . ' -w 3 -q 1 -m 16 -P icmp ' . escapeshellarg($ip) . ' 2>/dev/null | tail -1 | awk \'{print $1 "|" $4}\'';
        $query = '/usr/sbin/traceroute -w 3 -q 1 -m 16 -P icmp ' . escapeshellarg($ip) . ' 2>/dev/null | tail -1 | awk \'{print $1 "|" $4}\'';
        $hops = shell_exec($query);

        if(is_array($hops)) {
            $hops = $hops[0];
        }

        $hops = trim($hops);
        $explode = explode("|", $hops);

        if(!$ping) {
            return $explode[0];
        } else {
            return [
                'ping' => (float)$explode[1],
                'hops' => $explode[0]
            ];
        }
    }

    public function getBestRoute()
    {
        $api = new Api();
        $datacenters = $api->getDatacenters();

        $best = ['slug' => false, 'hops' => 100];
        if($datacenters) {
            foreach($datacenters as &$datacenter) {
                $route = $this->get($datacenter->ip, true);
                $datacenter->ping = $route['ping'];
                $datacenter->hops = $route['hops'];

                if($datacenter->hops < $best['hops']) {
                    $best['slug'] = $datacenter->slug;
                    $best['hops'] = $datacenter->hops;
                }
            }
        }

        return ['datacenters' => $datacenters, 'best' => $best];
    }
} 