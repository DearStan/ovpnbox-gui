<?php

namespace App\Services\Shell;


use App;
use App\Interfaces;
use App\Services\Helpers\PfSense;
use App\Services\Helpers\String;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Log;
use SimpleXMLElement;

class System {

    /**
     * Get path for the pfsense configuration file
     */
    public static function getPfsenseConfiguration()
    {
        if (App::environment() == 'local') {
            return storage_path('config.xml');
        } else {
            return '/cf/conf/config.xml';
        }
    }

    /**
     * Get the password generated for the bot user
     */
    public static function getBotPasswordPath()
    {
        if (App::environment() == 'local') {
            return storage_path('bot.passwd');
        } else {
            return '/usr/local/etc/ovpnse/bot.passwd';
        }
    }

    /*
     * Get the OpenVPN log file for a specific adapter
     */
    public static function getOpenVpnLog($adapter_id)
    {
        if (App::environment() == 'local') {
            return file_get_contents(storage_path('openvpn.log'));
        } else {
            return file_get_contents('/tmp/openvpn-' . $adapter_id . '.log');
        }
    }

    /**
     * Fetches the processor model
     *
     * @return string
     */
    public static function getCPU()
    {
        $rawData = trim(shell_exec('sysctl hw.model'));
        $explode = explode(":", $rawData);

        return trim($explode[1]);
    }

    /**
     * Fetches the temperature
     *
     * @return string
     */
    public static function getTemperature()
    {
        $temp_out = trim(shell_exec('sysctl dev.cpu.0.temperature'));
        $explode = explode(":", $temp_out);

        if(isset($explode[1])) {
            // Remove 'C' from the end
            return rtrim($explode[1], 'C');
        }

        return '-';
    }

    /**
     * Fetches installed RAM
     *
     * @return string
     */
    public static function getRAM()
    {
        $rawData = trim(shell_exec('sysctl hw.physmem'));
        $explode = explode(":", $rawData);

        return trim(round($explode[1]/1073741824));
    }

    /**
     * Get details in regards to the OVPN GUI commit
     */
    public static function getBoxCommit()
    {
        $version = Git::getBoxVersion();

        return [
            'link' => 'https://bitbucket.org/captaindoe/ovpnbox-gui/commits/' . $version,
            'short' => substr($version, 0, 7),
            'branch' => Git::getBoxBranch()
        ];
    }

    /**
     * Fetch information about all RAM modules
     */
    public static function getRamModels()
    {
        $speed = explode("Configured Clock Speed: ", trim(shell_exec('dmidecode --type 17  | grep -i "configured clock speed"')));
        $size = explode("Size: ", trim(shell_exec('dmidecode --type 17  | grep -i size')));
        $manufacturer = explode("Manufacturer: ", trim(shell_exec('dmidecode --type 17  | grep -i manufacturer')));
        $part = explode("Part Number: ", trim(shell_exec('dmidecode --type 17  | grep -i part')));
        $maximum = explode("Maximum Capacity: ", trim(shell_exec('dmidecode -t 16 | grep -i "Maximum Capacity"')));

        $ram = ['max' => $maximum[1], 'modules' => []];

        foreach($speed as $key => $line) {
            if(strpos($line, 'Unknown') === false && !empty($line)) {
                $ram['modules'][] = [
                    'manufacturer' => $manufacturer[$key],
                    'size' => $size[$key],
                    'part' => $part[$key],
                    'speed' => $speed[$key],
                ];
            }
        }

        return $ram;
    }

    /**
     * Fetches the total traffic volume for a specific interface
     *
     * @param $interface
     * @return array
     */
    public static function getTotalTraffic($interface)
    {
        exec("/sbin/pfctl -vvsI -i " . escapeshellarg($interface), $rawData);

        $traffic = [
            'download' => 0,
            'upload' => 0,
        ];

        if(empty($rawData)) {
            return $traffic;
        }

        $pf_in4_pass = preg_split("/ +/ ", $rawData[3]);
        $pf_out4_pass = preg_split("/ +/", $rawData[5]);
        $pf_in6_pass = preg_split("/ +/ ", $rawData[7]);
        $pf_out6_pass = preg_split("/ +/", $rawData[9]);

        $traffic['download'] = round(($pf_in4_pass[5] + $pf_in6_pass[5])/1073741824,2);
        $traffic['upload'] = round(($pf_out4_pass[5] + $pf_out6_pass[5])/1073741824,2);

        return $traffic;
    }

    /**
     * Get all network cards
     */
    public static function getNetworkCards()
    {
        $interfaces = Interfaces::all();
        $nics = [];

        foreach($interfaces as $interface) {
            if($interface->type == "openvpn") {
                continue;
            }

            if($interface->type == "wireless") {
                $exec = 'sysctl dev.ath.0.%desc';
            } else {
                $exec = 'sysctl dev.' . substr($interface->name, 0, -1) . '.' . substr($interface->name, -1) . '.%desc';
            }

            $rawData = trim(shell_exec($exec));
            $explode = explode(":", $rawData);

            if(isset($explode[1])) {
                $nics[$interface->type] = trim($explode[1]);
            }
        }

        return $nics;
    }

    /**
     * Get pfsense and system version
     */
    public static function getPfsenseVersion()
    {
        return trim(self::getBoxFirmwareVersion() . ' ' . php_uname("m"));
    }

    /**
     * Get pfsense version
     */
    public static function getBoxFirmwareVersion()
    {
        return trim(shell_exec('tail /etc/version'));
    }

    /**
     * Get latest available pfsense version available in OVPNs xmlrpc
     */
    public static function getAvailableFirmwareVersion()
    {
        try {
            $client = new Client([
                'defaults' => [
                    'verify' => true,
                    'headers' => [
                        'User-Agent' => 'OVPNbox ' . self::getPfsenseVersion(),
                    ]
                ]
            ]);

            $request = $client->get('https://boxrpc.ovpn.se/_updaters/amd64/version');

            return trim($request->getBody()->getContents());
        } catch(ClientException $ex) {
            Log::error('Misslyckat anrop för extern version', ['error' => (array)$ex]);
            return false;
        } catch(Exception $ex) {
            Log::error('Misslyckat anrop för extern version', ['error' => (array)$ex]);
            return false;
        }
    }

    /**
     * Check latest box GUI version
     *
     * @return bool|string
     */
    public static function getAvailableBoxVersion()
    {
        try {
            $client = new Client([
                'defaults' => [
                    'verify' => true,
                    'headers' => [
                        'User-Agent' => 'OVPNbox ' . self::getPfsenseVersion(),
                    ]
                ]
            ]);

            $request = $client->get('https://api.bitbucket.org/2.0/repositories/captaindoe/ovpnbox-gui/commits/' . urlencode(Git::getBoxBranch()));
            $data = $request->json(['object' => true]);

            return trim($data->values{0}->hash);
        } catch(ClientException $ex) {
            Log::error('Misslyckat anrop för extern GUI version', ['error' => (array)$ex]);
            return false;
        } catch(Exception $ex) {
            Log::error('Misslyckat anrop för extern GUI version', ['error' => (array)$ex]);
            return false;
        }
    }

    /**
     * Fetch system load information
     */
    public static function getSystemLoad()
    {
        // Fetch load
        return trim(shell_exec("uptime | awk -F'[a-z]:' '{ print $2}'"));
    }

    /**
     * Fetch system uptime in seconds
     */
    public static function getSystemUptime()
    {
        // Fetch uptime for OVPNbox
        return trim(shell_exec("expr `date +%s` - `sysctl kern.boottime | awk '{print $5}' | cut -d, -f1`"));
    }

    /**
     * Fetch wifi information from pfsense configuration file
     */
    public static function getWifiData()
    {
        // Load the pfsense configuration file
        $path = self::getPfsenseConfiguration();

        $xml = new SimpleXMLElement(
            file_get_contents($path)
        );

        foreach($xml->interfaces as $interfaces) {
            foreach($interfaces as $interface) {
                if(!isset($interface->wireless)) {
                    continue;
                }

                return $interface->wireless;
            }
        }

        return false;
    }

    /**
     * Check existence of wifi card
     */
    public static function getWifiCard()
    {
        $rawData = trim(shell_exec('sysctl dev.ath.0.%desc'));
        $explode = explode(":", $rawData);

        if(isset($explode[1]) && !empty($explode[1])) {
            return true;
        }

        return false;
    }

    public static function getWanInterface()
    {
        // Load the pfsense configuration file
        $path = self::getPfsenseConfiguration();

        $xml = new SimpleXMLElement(
            file_get_contents($path)
        );

        return (string)$xml->interfaces->wan->if;
    }

    /**
     * Get DHCP range for unsorted devices
     */
    public static function getLanRange()
    {
        // Load the pfsense configuration file
        $path = self::getPfsenseConfiguration();

        $xml = new SimpleXMLElement(
            file_get_contents($path)
        );

        return $xml->dhcpd->lan->range->from . ' - ' . $xml->dhcpd->lan->range->to;
    }

    /**
     * Get box LAN IP address
     */
    public static function getLanIp()
    {
        // Load the pfsense configuration file
        $path = self::getPfsenseConfiguration();

        $xml = new SimpleXMLElement(
            file_get_contents($path)
        );

        return (string)$xml->interfaces->lan->ipaddr;
    }

    /**
     * Fetches the uptime for a process
     *
     * @param $process
     * @return array|bool
     */
    public static function getProcessUptime($process)
    {

        $rawData = trim(shell_exec('ps x -e -o comm,etime'));
        $explode = explode("\n", $rawData);

        foreach($explode as $row) {

            // Skip all processes that don't match the requested one
            if(strpos($row, $process) === false) {
                continue;
            }

            $data = explode(" ", $row);

            foreach($data as $entry) {

                if(empty($entry)) {
                    continue;
                }

                if($entry == $process) {
                    continue;
                }

                return String::parseProcessUptime($entry);
            }

        }

        return '-';
    }

    /**
     * Checks if OpenVPN is running properly
     */
    public static function isOpenVpnRunning($client_id)
    {
        $pfsense = new PfSense();
        $result = $pfsense->getOpenVPNStatus($client_id);

        if($result == 'down') {
            return false;
        }

        return true;
    }

    /**
     * Checks if wifi is enabled.
     */
    public static function isWifiEnabled()
    {
        // Load the pfsense configuration file
        $path = self::getPfsenseConfiguration();

        $xml = new SimpleXMLElement(
            file_get_contents($path)
        );

        foreach($xml->interfaces as $interfaces) {
            foreach($interfaces as $interface) {
                if(!isset($interface->wireless)) {
                    continue;
                }

                if(isset($interface->enable) && $interface->enable) {
                    return true;
                }

                return false;
            }
        }

        return false;
    }

    /**
     * Reboot box
     */
    public static function reboot()
    {
        return shell_exec('reboot');
    }

    /**
     * Power off
     */
    public static function poweroff()
    {
        return shell_exec('poweroff');
    }

}