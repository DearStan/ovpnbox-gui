<?php

namespace App\Services\Network;
use App\Interfaces;

/**
 * Created by PhpStorm.
 * User: david
 * Date: 16-02-25
 * Time: 09:48
 */
class Adapter
{

    /**
     * @return array|mixed
     */
    public function get()
    {
        $ifconfig = new \Datasift\IfconfigParser\Parser\Darwin();
        $ifconfigOutput = shell_exec('/sbin/ifconfig');

        return $ifconfig->parse($ifconfigOutput);
    }

    public function getAvailableOpenVPN($group_id)
    {
        if($group_id == 1) {
            return 'ovpnse1';
        }

        $adapters = $this->get();
        $selected = Interfaces::where('type', 'openvpn')->get();
        $openvpn = [];

        foreach($selected as $entry)
        {
            $openvpn[] = $entry->name;
        }

        foreach($adapters as $adapter) {
            if($adapter['interface'] == "ovpnse1") {
                continue;
            }

            if(in_array($adapter['interface'], $openvpn)) {
                if(!isset($adapter['ip_address'])) {
                    return $adapter['interface'];
                }
            }
        }

        return false;
    }

    public function getStaticIP($interface)
    {
        $data = json_decode(shell_exec('curl --interface ' . escapeshellarg($interface) . ' https://www.ovpn.se/v1/api/client/ptr | python -m json.tool'));

        if(!is_null($data)) {
            if($data->status) {
                return ['ip' => $data->ip, 'ptr' => $data->ptr];
            }
        }

        return false;
    }
}