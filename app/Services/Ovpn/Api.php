<?php
/**
 * Created by PhpStorm.
 * User: davidwibergh
 * Date: 15-04-19
 * Time: 15:40
 */

namespace App\Services\Ovpn;

use App\Services\Shell\System;
use Cache;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Log;


class Api {

    protected function request($uri, $type, $body = false)
    {
        $payload = [
            'timeout' => 15
        ];

        if($body) {
            $payload['body'] = $body;
        }

        $client = new Client([
            'defaults' => [
                'verify' => true,
                'headers' => [
                    'User-Agent' => 'OVPNbox ' . System::getPfsenseVersion(),
                    'Accept'     => 'application/json',
                ]
            ]
        ]);

        try {

            if($type == 'GET') {
                $request = $client->get('https://www.ovpn.se/v1/api' . $uri, $payload);
            } else {
                $request = $client->post('https://www.ovpn.se/v1/api' . $uri, $payload);
            }
            return $request->json(['object' => true]);

        } catch(ClientException $ex) {
            Log::error('Misslyckat anrop', ['uri' => $uri, 'error' => (array)$ex]);
            return false;
        } catch(Exception $ex) {
            Log::error('Misslyckat anrop', ['uri' => $uri, 'error' => (array)$ex]);
            return false;
        }
    }

    public function login($username, $password)
    {
        return $this->request('/client', 'POST', ['username' => $username,'password' => $password]);
    }

    public function getDatacenters()
    {
        return Cache::get('datacenters', function() {
            $datacenters = $this->request('/client/datacenters', 'GET');

            if($datacenters) {
                Cache::put('datacenters', $datacenters, 1440);
            }
            return $datacenters;
        });
    }

    public function getServers()
    {
        return Cache::get('servers', function() {
            $servers = $this->request('/client/servers', 'GET');

            if($servers) {
                Cache::put('servers', $servers, 120);
            }
            return $servers;
        });
    }

    public function getServersInDatacenter($datacenter)
    {
        return $this->request('/client/servers/' . $datacenter, 'GET');
    }

    public function getBestServersInDatacenter($datacenter, $results = 3)
    {
        $servers = $this->getServersInDatacenter($datacenter);

        if(!$servers) {
            return false;
        }

        // Sort the array with ascending so we get the datacenter with the lowest amount of hops
        $sort = array();
        foreach ($servers as $key => $row) {

            if(isset($row->currentLoad->online)) {
                if(!$row->currentLoad->online) {
                    unset($servers[$key]);
                    continue;
                }
                $sort[$key]  = $row->currentLoad->bandwidth;
            } else {
                Cache::forget('datacenters');
                Cache::forget('servers');
                Log::debug('Weird information returned', ['data' => $row, 'key' => $key]);
                $sort[$key] = 9;
            }
        }

        array_multisort($sort, SORT_ASC, $servers);

        if($results > 1) {
            $return = [];

            $x = 0;
            while($x < $results) {
                $return[] = $servers[$x];
                $x++;
            }

            return $return;
        } else {
            return $servers[0];
        }
    }

    public function sendPing($payload)
    {
        return $this->request('/client/box', 'POST', $payload);
    }

    public function sendBoxReset($payload)
    {
        return $this->request('/client/box/reset', 'POST', $payload);
    }
} 