<?php

namespace App\Console\Commands;

use App\Group;
use App\Traffic;
use Illuminate\Console\Command;
use Log;

class OpenVpnStats extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'openvpn:stats';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetches traffic statistics for all connected interfaces';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $connected = Group::connected()->get();

        if($connected->isEmpty()) {
            return true;
        }

        foreach($connected as $group) {

            $statistics = json_decode(shell_exec('/usr/local/bin/ovpn_get_interface_stats.py -i ' . $group->adapter->name));

            if(!isset($statistics->in_bytes)) {
                Log::error('Failed to execute interface stats', ['group' => (array)$group, 'statistics' => $statistics]);
                continue;
            }

            $history = Traffic::where('group_id', $group->id)->orderBy('id', 'desc')->first();

            if(is_null($history)) {
                $in = round($statistics->in_bytes/1048576);
                $out = round($statistics->out_bytes/1048576);
            } else {

                $in = round((($statistics->in_bytes/1024/1024)-$history->total_in_megabytes));
                $out = round((($statistics->out_bytes/1024/1024)-$history->total_out_megabytes));

                if($in < 0) {
                    $in = 0;
                }

                if($out < 0) {
                    $out = 0;
                }
            }

            Traffic::create([
                'group_id' => $group->id,
                'in_megabytes' => $in,
                'out_megabytes' => $out,
                'total_in_megabytes' => round($statistics->in_bytes/1048576),
                'total_out_megabytes' => round($statistics->out_bytes/1048576)
            ]);

            echo 'Stored statistics for ' . $group->name . "\n";
        }

        return true;
    }
}
