<?php

namespace App\Console\Commands;

use App\Traffic;
use Carbon\Carbon;
use Illuminate\Console\Command;

class OpenVpnPruneStats extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'openvpn:prune';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Removes statistics older than 31 days';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Traffic::whereDate('created_at', '<=', Carbon::today()->subDays(31)->toDateString())->delete();;
    }
}
