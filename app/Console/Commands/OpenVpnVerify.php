<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class OpenVpnVerify extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'openvpn:verify';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Verifies that groups are still connected';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
    }
}
