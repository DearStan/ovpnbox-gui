<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\RedisSubscribe::class,
        \App\Console\Commands\OpenVpnStats::class,
        \App\Console\Commands\OpenVpnVerify::class,
        \App\Console\Commands\OpenVpnPruneStats::class,
        \App\Console\Commands\SendBoxPing::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // Verify that no groups have been disconnected
        //$schedule->command('openvpn:verify')->everyFiveMinutes();

        // Gather traffic stats
        $schedule->command('openvpn:stats')->everyFiveMinutes();

        // Prune old stats
        $schedule->command('openvpn:prune')->daily();

        // Send ping
        $schedule->command('box:ping')->daily();
    }
}
