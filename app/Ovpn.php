<?php

namespace App;

use App\Services\Helpers\String;
use Illuminate\Database\Eloquent\Model;

class Ovpn extends Model
{
    //
    protected $table = 'ovpn';
    protected $fillable = ['username', 'password', 'default', 'filtering', 'multihop', 'public_ipv4', 'ports'];
    protected $casts = [
        'ports' => 'array',
    ];

    public function getDefaultAttribute($value)
    {
        return String::parseTimestamp($value);
    }

    public function getFilteringAttribute($value)
    {
        return String::parseTimestamp($value);
    }

    public function getMultihopAttribute($value)
    {
        return String::parseTimestamp($value);
    }

    public function getPublicIpv4Attribute($value)
    {
        return String::parseTimestamp($value);
    }

    public function getAddonsAttribute()
    {
        $addons = [];

        if(!is_null($this->attributes['filtering'])) {
            $addons[] = 'filtering';
        }

        if(!is_null($this->attributes['multihop'])) {
            $addons[] = 'multihop';
        }

        if(!is_null($this->attributes['public_ipv4'])) {
            $addons[] = 'public-ipv4';
        }

        if(empty($addons)) {
            return false;
        }

        return $addons;
    }
}
