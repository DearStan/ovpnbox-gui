<?php

namespace App\Http\Controllers;

use App\Group;
use App\Traffic;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Response;

class StatisticsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $meta = ['page' => 'stats', 'title' => trans('general.statistics')];
        $connected = Group::connected()->get();

        return view('pages.stats', compact('meta', 'connected'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, $span = 'hour')
    {
        if($span == 'day') {
            $collection = Traffic::where('group_id', $id)->day();
            $text = trans('stats.one_day');
        } else if($span == 'week') {
            $collection = Traffic::where('group_id', $id)->week();
            $text = trans('stats.one_week');
        } else {
            $collection = Traffic::where('group_id', $id)->month();
            $text = trans('stats.one_month');
        }

        $traffic = $collection->get();
        $result = [];

        if(!$traffic->count()) {
            return Response::json(['port_range' => [trans('stats.no_stats_found')]], 422);
        }

        foreach($traffic as $record) {
            $result[] = [
                'input' => (float)$record->in_megabytes,
                'output' => (float)($record->out_megabytes*-1),
                'date'   => $record->created_at->toRfc2822String()
            ];
        }

        return Response::json([
            'traffic' => $result,
            'summary' => [
                'input' => $collection->sum('in_megabytes'),
                'output' => $collection->sum('out_megabytes')
            ],
            'text' => $text
         ]);
    }
}
