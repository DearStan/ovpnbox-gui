<?php

namespace App\Http\Controllers;

use App\Interfaces;
use App\Services\Shell\Git;
use App\Services\Shell\System;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;

class SystemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $meta = ['page' => 'system', 'title' => trans('general.system')];
        $interfaces = Interfaces::all();

        $traffic = [];

        if(!is_null($interfaces)) {
            foreach($interfaces as $interface) {

                if($interface->type == 'wireless') {
                    continue;
                }

                $traffic[$interface->name] = System::getTotalTraffic($interface->name);
            }
        }

        if (App::environment('local')) {
            $system = [
                'pfsense' => php_uname("m"),
                'freebsd' => php_uname("r"),
                'version' => [
                    'link' => 'https://bitbucket.org/captaindoe/ovpnbox-gui/commits/',
                    'short' => 'dummy',
                    'branch' => 'dummy'
                ],
                'php' => phpversion(),
                'web' => $_SERVER['SERVER_SOFTWARE'],
                'hardware' => [
                    'uptime' => System::getSystemUptime(),
                    'load' => 'dummy data',
                    'nics' => ['wan' => 'Intel dummy data', 'wireless' => 'Intel wireless dummy data', 'lan' => 'Intel dummy data' ],
                    'ram' => ['max' => '32 GB', 'modules' => [
                        [
                            'manufacturer' => 'Dummy manufacturer',
                            'size' => '2 GB',
                            'part' => 'DUMMY-RAM-01',
                            'speed' => '1600 MHz'
                        ],
                        [
                            'manufacturer' => 'Dummy manufacturer 2',
                            'size' => '2 GB',
                            'part' => 'DUMMY-RAM-02',
                            'speed' => '1600 MHz'
                        ],
                    ]],
                    'temperature' => 'dummy data',
                    'processor' => System::getCPU()
                ]
            ];
        } else {
            $system = [
                'pfsense' => System::getPfsenseVersion(),
                'freebsd' => php_uname("r"),
                'version' => System::getBoxCommit(),
                'php' => phpversion(),
                'web' => $_SERVER['SERVER_SOFTWARE'],
                'hardware' => [
                    'uptime' => System::getSystemUptime(),
                    'load' => System::getSystemLoad(),
                    'nics' => System::getNetworkCards(),
                    'ram' => System::getRamModels(),
                    'temperature' => System::getTemperature(),
                    'processor' => System::getCPU()
                ]
            ];
        }

        $version = [
            'firmware' => Git::isThereNewFirmwareAvailable(),
            'gui' => Git::isThereNewGuiAvailable()
        ];

        return view('pages.system', compact('meta', 'interfaces', 'traffic', 'system', 'version'));
    }

    public function reboot()
    {
        return System::reboot();
    }

    public function poweroff()
    {
        return System::poweroff();
    }

    public function update()
    {

    }
}
