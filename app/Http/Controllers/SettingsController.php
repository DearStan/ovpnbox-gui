<?php

namespace App\Http\Controllers;

use App;
use App\Group;
use App\Ovpn;
use App\Services\Ovpn\Api;
use App\User;
use Auth;
use Exception;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Response;

class SettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $meta = ['page' => 'settings', 'title' => trans('general.settings')];

        $ovpn = Ovpn::first();
        $user = User::first();

        return view('pages.settings', compact('meta', 'ovpn', 'user'));
    }

    /**
     * Update the administrator account
     *
     * @return \Illuminate\Http\Response
     */
    public function updateAccount(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required|min:6',
            'password_confirmation' => 'required|same:password',
            'language' => 'required|in:sv,en'
        ]);

        try {

            $user = User::first();

            $user->update([
                'name' => $request->get('name'),
                'email' => $request->get('email'),
                'password' => password_hash($request->get('password'), PASSWORD_BCRYPT, ["cost" => 12]),
                'language' => $request->get('language')
            ]);

            // getting login info with new account.
            if (!Auth::attempt([
                'name' => $request->get('name'),
                'password' => $request->get('password')
            ], true)) {
                return Response::json(['credentials' => [trans('auth.failed')]], 422);
            }

            // Localize unsoruted group
            App::setLocale($request->get('language'));
            Group::hidden()->update(['name' => trans('setup.unsorted_devices')]);

        } catch(Exception $ex) {
            return Response::json(['credentials' => [trans('auth.failed')]], 422);
        }

        return Response::json(['status' => true]);
    }

    /**
     * Update OVPN account details
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateOvpn(Request $request)
    {
        $this->validate($request, [
            'username' => 'required',
            'password' => 'required',
        ]);

        $api = new Api();
        $result = $api->login($request->get('username'),$request->get('password'));

        if($result) {
            $ovpn = Ovpn::first();
            $ovpn->update([
                'username' => $request->get('username'),
                'password' => $request->get('password'),
                'default' => $result->timestamp,
                'filtering' => $result->addons->filtering->timestamp,
                'multihop' => $result->addons->multihop->timestamp,
                'public_ipv4' => $result->addons->{'public-ipv4'}->timestamp,
                'ports' => $result->portforward
            ]);

            return Response::json(['status' => true]);
        }

        return Response::json(['credentials' => [trans('auth.failed')]], 422);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
