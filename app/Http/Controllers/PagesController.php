<?php

namespace App\Http\Controllers;

use App;
use App\Ovpn;
use App\Services\Ovpn\Api;
use App\Services\Shell\Traceroute;
use Auth;
use Faker\Provider\Uuid;
use Illuminate\Http\Request;
use Response;

class PagesController extends Controller
{

    public function getLogin()
    {
        $meta = ['page' => 'login', 'title' => trans('general.signin')];
        return view('auth.login', compact('meta'));
    }

    public function postLogin(Request $request)
    {

        $this->validate($request, [
            'name' => 'required',
            'password' => 'required',
            'remember' => 'required',
        ]);


        if($request->get('remember') == "on") {
            $remember = true;
        } else {
            $remember = false;
        }

        if (!Auth::attempt(['name' => $request->get('name'), 'password' => $request->get('password')], $remember)) {
            return Response::json(['credentials' => [trans('auth.failed')]], 422);
        }

        return Response::json(['status' => true]);
    }

    public function getOvpn()
    {
        $meta = ['page' => 'ovpn', 'title' => 'OVPN'];
        $account = Ovpn::find(1);
        $traceroute = new Traceroute();
        $api = new Api();

        $datacenters = $traceroute->getBestRoute();
        $servers = $api->getBestServersInDatacenter($datacenters['best']['slug']);

        return view('pages.ovpn', compact('meta', 'account', 'datacenters', 'servers'));
    }

    public function putOvpn()
    {
        $account = Ovpn::find(1);
        $api = new Api();
        $result = $api->login($account->username,$account->password);

        if($result) {
            $account->update([
                'default' => $result->timestamp,
                'filtering' => $result->addons->filtering->timestamp,
                'multihop' => $result->addons->multihop->timestamp,
                'public_ipv4' => $result->addons->{'public-ipv4'}->timestamp,
                'ports' => $result->portforward
            ]);

            return Response::json(['status' => true]);
        }

        return Response::json(['credentials' => [trans('ovpn.failed_fetch')]], 422);
    }

    public function getDemo()
    {
        /*$pfsense = new App\Services\Helpers\PfSense();
        $pfsense->updatePfSense();*/
    }


}

