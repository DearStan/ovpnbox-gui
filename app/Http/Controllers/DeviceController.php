<?php

namespace App\Http\Controllers;

use App;
use App\Device;
use App\Group;
use App\Port;
use App\Services\Helpers\Config;
use App\Services\Helpers\PfSense;
use App\Services\Network\StaticIp;
use App\Services\Shell\System;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Log;
use Response;

class DeviceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $meta = ['page' => 'devices', 'title' => trans('general.devices')];
        $groups = Group::visible()->get();

        if (App::environment('local')) {
            $unsorted = [['hostname' =>  'sdg', 'ip' => '10.220.0.28', 'mac' => '62:d2:je:00:2d:70'], ['hostname' =>  'sfhjdjd', 'ip' => '10.220.0.125', 'mac' => '62:d2:ee:00:2d:70'], ['hostname' =>  'shdfjdj', 'ip' => '10.220.0.165', 'mac' => '22:d2:je:00:2d:70'], ['hostname' =>  'sfhdjd', 'ip' => '10.220.0.185', 'mac' => '96:2f:d4:7b:26:0c'], ['hostname' =>  'sdg', 'ip' => '10.220.0.30', 'mac' => '72:05:26:e8:62:26']];
        } else {
            $unsorted = App\Services\Shell\DhcpLeases::get();
        }

        $macs = [];
        if(!is_null($groups)) {
            foreach($groups as $group) {
                if(!$group->devices->isEmpty()) {
                    foreach ($group->devices as $device) {
                        $macs[] = $device->mac;
                    }
                }
            }
        }

        if(!empty($unsorted)) {
            foreach($unsorted as $key => $device) {
                if(isset($device['mac'])) {
                    if(in_array($device['mac'], $macs)) {
                        unset($unsorted[$key]);
                    }
                }
            }

            if(!empty($unsorted)) {
                sort($unsorted);
            }
        }

        $hidden = Group::connected()->hidden()->first();
        $devices = Device::all();

        return view('pages.devices', compact('meta', 'groups', 'unsorted', 'hidden', 'devices'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'mac' => 'required',
            'group' => 'required',
        ]);

        $mac = $request->get('mac');
        $name = $request->get('name');
        $group = $request->get('group');

        if($group == 'create') {
            $model = Group::create(['name' => trans('devices.group') . ' ' . (Group::all()->count())]);

        } else if($group == 'choose') {
            return Response::json(['group' => [trans('devices.choose_existing_or_not')]], 422);
        } else {
            $model = Group::findOrFail($group);
        }

        $ip = 2;
        $max = 100;

        while($ip <= $max) {

            $full = substr(System::getLanIp(), 0, -1) . $ip;
            $long = sprintf("%u", ip2long($full));

            $available = Device::where('ip', $long)->first();

            if(!is_null($available)) {
                $ip++;
                continue;
            }

            $already_named = Device::where('name', trim($name))->get();

            if($already_named->count() > 0) {
                $name = $name . ' ' . ($already_named->count()+1);
            }

            $create = Device::create([
                'name' => trim($name),
                'mac' => $mac,
                'ip' => $full,
                'group_id' => $model->id
            ]);

            $pfsense = new App\Services\Helpers\PfSense();
            $result = $pfsense->setIP($mac, $name, $full);

            if(!$result) {
                return Response::json(['failed' => [trans('devices.failed_assign_static')]], 422);
            }

            Config::generate();
            Config::flushFirewall();

            return Response::json([
                'id' => $create->id,
                'group' => [
                    'id' => $model->id,
                    'title' => $model->name
                ],
                'group_array' => $model->toArray(),
                'device' => $name,
                'ip' => $full,
                'mac' => $mac
            ], 200);

        }

        Config::generate();
        Config::flushFirewall();

        return Response::json(['status' => true], 200);
    }

    public function getStatus(Request $request, $id)
    {
        $device = Device::find($id);

        if($device->isUp()) {
            return Response::json(['status' => true], 200);
        } else {
            return Response::json(['status' => false], 200);
        }
    }

    /*
     * Add a device to bypass status
     */
    public function storeBypass(Request $request, $id)
    {
        Device::find($id)->update(['bypass' => true]);

        Config::generate();
        Config::flushFirewall();

        return Response::json(['status' => true], 200);
    }

    /*
     * Remove a device from bypass status.
     */
    public function destroyBypass(Request $request, $id)
    {
        Device::find($id)->update(['bypass' => false]);

        Config::generate();
        Config::flushFirewall();

        return Response::json(['status' => true], 200);
    }

    /*
     * Move device to another group
     */
    public function moveDevice(Request $request, $id)
    {
        $this->validate($request, [
            'group' => 'required|exists:groups,id',
        ]);

        Device::find($id)->update(['group_id' => $request->get('group')]);

        Config::generate();
        Config::flushFirewall();

        return Response::json(['status' => true], 200);
    }

    /**
     * Update the specified device in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        $name = trim($request->get('name'));

        $already_named = Device::where('name', $name)->get();

        if($already_named->count() > 0) {
            $name = $name . ' ' . ($already_named->count()+1);
        }

        $device = Device::find($id)->update(['name' => trim($name)]);

        if(!$device) {
            return Response::json(['failed' => [trans('devices.failed_update_device_name')]], 422);
        }

        return Response::json(['status' => true, 'name' => $name], 200);
    }

    /**
     * Remove the specified device from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Find device
        $device = Device::find($id);

        $pfsense = new PfSense();
        $pfsense->removeIP($device->mac);

        $device->delete();

        Port::where('device_id', $id)->delete();

        Config::generate();
        Config::flushFirewall();

        return Response::json(['status' => true], 200);

    }
}
