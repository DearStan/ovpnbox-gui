<?php

namespace App\Http\Controllers;

use App\Connection;
use App\Group;
use App\Interfaces;
use App\Ovpn;
use App\Services\Helpers\Config;
use App\Services\Helpers\OpenVPN;
use App\Services\Helpers\PfSense;
use App\Services\Helpers\String;
use App\Services\Network\Adapter;
use App\Services\Network\Socket;
use App\Services\Ovpn\Api;
use App\Services\Ovpn\Dashboard;
use App\Services\Shell\System;
use App\Services\Shell\Traceroute;
use App\Traffic;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redis;
use Log;
use Response;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $meta = ['page' => 'index', 'title' => trans('general.connection')];

        $ovpn = Ovpn::first();
        $disconnected = Group::disconnected()->get();
        $connected = Group::connected()->get();
        $api = new Api();
        $servers = $api->getServers();

        return view('pages.dashboard', compact('meta', 'ovpn', 'disconnected', 'connected', 'servers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $connect = OpenVPN::start($request->all());

        if(isset($connect['error'])) {
            return Response::json($connect['error'], 422);
        }

        return Response::make($connect['ip']);
    }

    /**
     * Display the specified resource log.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $connection = Connection::where('group_id', $id)->active()->first();

        if(is_null($connection)) {
            return Response::json(['connection_failed' => [trans('dashboard.failed_to_fetch_connection')]], 422);
        }

        return Response::make(nl2br(System::getOpenVpnLog($connection->directives['ids']['adapter_id'])));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $connection = Connection::where('group_id', $id)->active()->first();

        $stop = OpenVPN::stop($id);

        if(isset($stop['error'])) {
            return Response::json($stop['error'], 422);
        }

        $connect = OpenVPN::start($connection->directives['input']);

        if(isset($connect['error'])) {
            return Response::json($connect['error'], 422);
        }

        return Response::make();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $stop = OpenVPN::stop($id);

        if(isset($stop['error'])) {
            return Response::json($stop['error'], 422);
        }

        return Response::make();
    }
}
