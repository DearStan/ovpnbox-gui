<?php

namespace App\Http\Controllers;

use App\Group;
use App\Port;
use App\Services\Helpers\Config;
use App\Services\Helpers\PfSense;
use App\Services\Network\Socket;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Response;

class GroupController extends Controller
{
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        Group::find($id)->update(['name' => $request->get('name')]);

        return Response::json(['status' => true], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Find group
        $group = Group::find($id);


        if(!is_null($group->connection()->active()->first())) {
            return Response::json(['connected' => [trans('devices.cant_remove_group')]], 422);
        }

        Socket::send('devices', trans('devices.group_found'));

        if(!$group->devices->isEmpty()) {

            Socket::send('devices', trans('devices.devices_found'));
            $pfsense = new PfSense();

            foreach($group->devices as $device) {
                Socket::send('devices', trans('devices.device_found') . ' ' . $device->name . '. ' . trans('devices.removing_static_ip'));
                $pfsense->removeIP($device->mac);
                Port::where('device_id', $device->id)->delete();
            }

            Socket::send('devices', trans('devices.all_devices_removed'));

        } else {
            Socket::send('devices', trans('devices.no_devices'));
        }

        Socket::send('devices', trans('devices.removing_group'));
        $group->delete();

        Config::generate();
        Config::flushFirewall();

        return Response::json(['status' => true], 200);
    }
}
