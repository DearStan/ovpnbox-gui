<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
*/

Route::get('setup/{step1?}', ['middleware' => 'setup', 'as' => 'setup', 'uses' => 'SetupController@getSetup']);
Route::post('setup/3', ['middleware' => 'setup','as' => 'create.account', 'uses' => 'SetupController@postSetupAccount']);
Route::post('setup/2', ['middleware' => 'setup', 'uses' => 'SetupController@postOvpnLogin']);
Route::post('setup/1', ['middleware' => 'setup', 'uses' => 'SetupController@postIntialSetup']);

Route::get('login', 'PagesController@getLogin');
Route::post('login', 'PagesController@postLogin');
Route::get('logout', 'Auth\AuthController@getLogout');

// Password reset link request routes...
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');

// Password reset routes...
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');

Route::group(['middleware' => ['auth', 'language']], function () {
    Route::get('ovpn', ['as' => 'ovpn', 'uses' => 'PagesController@getOvpn']);
    Route::put('ovpn', 'PagesController@putOvpn');

    Route::get('wireless', ['as' => 'wireless', 'uses' => 'WirelessController@index']);
    Route::post('wireless', ['middleware' => 'auth', 'uses' => 'WirelessController@store']);

    Route::resource('/', 'DashboardController', ['as' => 'home', 'only' => ['index', 'store']]);
    Route::get('connection/{id}', 'DashboardController@show');
    Route::put('connection/{id}', 'DashboardController@update');
    Route::delete('connection/{id}', 'DashboardController@destroy');

    Route::resource('port', 'PortController', ['only' => ['index', 'store', 'destroy']]);

    Route::get('stats', 'StatisticsController@index');
    Route::get('stats/{id}/{interval}', 'StatisticsController@show');

    Route::resource('devices', 'DeviceController', ['only' => ['index', 'store', 'destroy', 'update']]);
    Route::group(['prefix' => 'devices'], function () {
        Route::put('{id}/move', 'DeviceController@moveDevice');
        Route::get('{id}/status', 'DeviceController@getStatus');
        Route::post('{id}/bypass', 'DeviceController@storeBypass');
        Route::delete('{id}/bypass', 'DeviceController@destroyBypass');
    });

    Route::resource('groups', 'GroupController', ['only' => ['destroy', 'update']]);

    Route::group(['prefix' => 'system'], function () {
        Route::get('/', ['as' => 'system', 'uses' => 'SystemController@index']);
        Route::post('reboot', 'SystemController@reboot');
        Route::post('poweroff', 'SystemController@poweroff');
    });

    Route::group(['prefix' => 'settings'], function () {
        Route::get('/', ['as' => 'settings.index', 'uses' => 'SettingsController@index']);
        Route::put('admin', ['as' => 'settings.admin', 'uses' => 'SettingsController@updateAccount']);
        Route::put('ovpn', ['as' => 'settings.ovpn', 'uses' => 'SettingsController@updateOvpn']);
    });
});

Route::get('demo', ['as' => 'test', 'uses' => 'PagesController@getDemo']);