<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Traffic extends Model
{
    protected $fillable = [
        'group_id',
        'in_megabytes',
        'out_megabytes',
        'total_in_megabytes',
        'total_out_megabytes'
    ];

    /*
     * Each traffic record belongs to an interface
     */
    public function group()
    {
        return $this->belongsTo('App\Group', 'group_id');
    }

    /*
     * Fetch all traffic records for the last day
     */
    public function scopeDay($query)
    {
        return $query->where('created_at', '>=', Carbon::now()->subDay());
    }

    /*
     * Fetch all traffic records for the last week
     */
    public function scopeWeek($query)
    {
        return $query->where('created_at', '>=', Carbon::now()->subDays(7));
    }

    /*
     * Fetch all traffic records for the last month
     */
    public function scopeMonth($query)
    {
        return $query->where('created_at', '>=', Carbon::now()->subDays(31));
    }
}
