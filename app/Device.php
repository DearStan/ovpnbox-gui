<?php

namespace App;

use App\Services\Network\Ping;
use Illuminate\Database\Eloquent\Model;

class Device extends Model
{
    //
    protected $fillable = ['mac', 'ip', 'group_id', 'name', 'bypass'];
    protected $casts = [
        'bypass' => 'boolean',
    ];

    /*
     * Each device belongs to a group
     */
    public function group() {
        return $this->belongsTo('App\Group');
    }

    /*
     * Each devices has multiple ports
     */
    public function ports()
    {
        return $this->hasMany('App\Port', 'device_id');
    }

    public function getIpAttribute($value)
    {
        return long2ip($value);
    }

    public function getNameAttribute($value)
    {
        if(is_null($value)) {
            return 'Ej angivet';
        }
        return $value;
    }

    public function setIpAttribute($value)
    {
        $this->attributes['ip'] = sprintf("%u", ip2long($value));
    }

    public function isUp()
    {
        return Ping::get($this->attributes['ip']);
    }


}
