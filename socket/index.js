var server = require('http').Server();
var io = require('socket.io')(server);
var Redis = require('ioredis');
var redis = new Redis();

redis.subscribe('ovpn');

redis.on('message', function(channel, message) {
    message = JSON.parse(message);
    console.log(channel + ':' + message.event);
    console.log(message.data);
    io.emit(channel + ':' + message.event, message.data);
});

server.listen(3000);