<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropProxyFromOvpnTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ovpn', function (Blueprint $table) {
            $table->dropColumn('proxy');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ovpn', function (Blueprint $table) {
            $table->integer('proxy')->unsigned()->nullable();
        });
    }
}
