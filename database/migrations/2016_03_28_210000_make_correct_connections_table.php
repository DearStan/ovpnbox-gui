<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeCorrectConnectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('connections', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('group_id');
            $table->integer('interface_id');
            $table->json('directives');
            $table->timestamps();
            $table->timestamp('disconnected_at')->nullable();
            $table->foreign('group_id')->references('id')->on('groups');
            $table->foreign('interface_id')->references('id')->on('interfaces');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('connections');
    }
}
