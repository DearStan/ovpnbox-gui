$(function () {

    $("#name").focus();

    $("#create").submit(function(event) {
        event.preventDefault();

        // Hide error message
        var error = $(".error"), name = $("#name"), email = $("#email"),
            password = $("#password"), confirmation = $("#password_confirmation"),
            language = $("#language"), button = $("#create").find('button');

        error.addClass('hidden');
        displayMessage('info', $.i18n('common.creating-account'), $.i18n('common.verifying-and-creating'));

        // Update button
        button.html('<i class="fa fa-circle-o-notch fa-spin"></i> ' + $.i18n('common.verifying'));

        $.ajax({
            type: "POST",
            url:  "/setup/3",
            data: {
                _token: $('input[name=_token]').val(),
                name: name.val(),
                email: email.val(),
                password: password.val(),
                password_confirmation: confirmation.val(),
                language: language.val()
            },
            async: true,
            cache: false,
            timeout:120000,
            success: function () {
                window.location = '/';
            },
            error: function(xhr, textStatus, errorThrown ) {
                try {
                    var err = JSON.parse(xhr.responseText);
                } catch(error) {
                    var err = [{"error": [$.i18n('common.technical-error')]}];
                }

                displayMessage('error', $.i18n('common.error'), err);
                button.html($.i18n('common.create-account'));
            }
        });
    });

});
