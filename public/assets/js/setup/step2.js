$(function () {

    $("#username").focus();

    $("#login").submit(function(event) {
        event.preventDefault();

        // Hide error message
        var error = $(".error"), username = $("#username"), password = $("#password"), button = $("#login").find('button');
        error.addClass('hidden');

        displayMessage('info', $.i18n('common.signing-in'), $.i18n('common.verifying-credentials'));

        // Update button
        button.html('<i class="fa fa-circle-o-notch fa-spin"></i> ' + $.i18n('common.verifying'));

        $.ajax({
            type: "POST",
            url:  "/setup/2",
            data: {
                _token: $('input[name=_token]').val(),
                username: username.val(),
                password: password.val()
            },
            async: true,
            cache: false,
            timeout:120000,
            success: function () {
                window.location = '/setup/3';
            },
            error: function(xhr, textStatus, errorThrown ) {
                try {
                    var err = JSON.parse(xhr.responseText);
                } catch(error) {
                    var err = [{"error": [$.i18n('common.technical-error')]}];
                }

                displayMessage('error', $.i18n('common.error'), err);
                button.html($.i18n('common.sign-in'));
            }
        });
    });

});
