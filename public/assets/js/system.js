$(function () {

    /**
     * Starta om
     */
    $("#reboot").submit(function (event) {
        event.preventDefault();

        displayMessage('info', $.i18n('system.rebooting'), $.i18n('system.reboot'));

        // Button
        $('button[type=submit]').prop("disabled", true);

        $.ajax({
            type: "POST",
            url: "/system/reboot",
            data: {
                _token: $('input[name=_token]').val()
            },
            async: true,
            cache: false,
            timeout: 120000
        });
    });

    /**
     * Stäng av
     */
    $("#poweroff").submit(function (event) {
        event.preventDefault();

        displayMessage('info', $.i18n('system.poweroff'), $.i18n('system.poweroff-working'));

        // Update button
        $('button[type=submit]').prop("disabled", true);

        $.ajax({
            type: "POST",
            url: "/system/poweroff",
            data: {
                _token: $('input[name=_token]').val()
            },
            async: true,
            cache: false,
            timeout: 120000
        });
    });
});