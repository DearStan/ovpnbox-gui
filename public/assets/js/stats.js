$(function () {

    // Form that gets executed when user tries to delete a port forward
    $("body").on('click', '.graph', function() {
        loadGraph($(this).data('group'), $(this).data('interval'));
    });
});

function loadGraph(group_id, interval) {

    $.ajax({
        type: "GET",
        url: "/stats/" + group_id + "/" + interval,
        data: {
            _token: $("#token").data('token')
        },
        async: true,
        cache: false,
        timeout: 0,
        success: function (retval) {

            $("#title_" + group_id).text(retval.text);
            $("#traffic_input_" + group_id).text(retval.summary.input.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' '));
            $("#traffic_output_" + group_id).text(retval.summary.output.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' '));

            var size = Object.size(retval.traffic);

            Highcharts.setOptions({
                global: {
                    timezoneOffset: -120
                }
            });

            // User is not accessing through mobile. Create the chart
            $('#graph_' + group_id).highcharts('StockChart', {
                chart: {
                    defaultSeriesType: 'areaspline'
                },
                tooltip: {
                    formatter: function () {
                        var s = '<b>' + new Date(this.x).toLocaleString() + '</b>', val = 0;

                        $.each(this.points, function () {
                            if(this.y < 0) {
                                val = (this.y*-1);
                            } else {
                                val = this.y;
                            }

                            s += '<br/><span style="color:' + this.series.color + '">•</span> ' + this.series.name + ': ' +
                                val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ');
                        });

                        return s;
                    },
                    shared: true
                },
                rangeSelector: {
                    enabled: false
                },
                navigator: {
                    enabled: false
                },
                scrollbar: {
                    enabled: false
                },
                title: {
                    text: null
                },
                yAxis: {
                    title: {
                        text: null
                    }
                },
                xAxis: {
                    type: 'datetime'
                },
                series: [{
                    name: $.i18n('stats.downloaded') + ' (MB)',
                    color: '#1EB300',
                    fillOpacity: 0.2,
                    data: (function () {
                        var data = [];

                        for (i = 0; i < size; i++) {
                            data.push([
                                (new Date(retval.traffic[i].date)).getTime(),
                                retval.traffic[i].input
                            ]);
                        }

                        return data;
                    }())
                }, {
                    name: $.i18n('stats.uploaded') + ' (MB)',
                    color: '#6D88AD',
                    fillOpacity: 0.2,
                    data: (function () {
                        var data = [];

                        for (i = 0; i < size; i++) {
                            data.push([
                                (new Date(retval.traffic[i].date)).getTime(),
                                retval.traffic[i].output
                            ]);
                        }

                        return data;
                    }())
                }
                ]
            });
        }
    });
}

Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};