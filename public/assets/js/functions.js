var loadLocale = function(locale) {
    var data = "LOCALE_" + locale.toUpperCase();
    return window[data];
};

function hideMessage()
{
    $(".info").addClass('hidden');
    $(".error").addClass('hidden');
    $(".success").addClass('hidden');
}

function localize(locale) {
    var localeData = loadLocale(locale);

    // if locale doesn't exist use en locale
    if (!localeData) {
        locale = 'en';
        localeData = loadLocale(locale);
    }

    $.i18n.locale = locale;
    $.i18n().load(localeData, locale);
    return true;
}

function displayMessage(type, title, message) {

    var display = '', element, hide, icon;

    if(type == 'info') {
        element = $(".info");
        hide = ["success", "error"];
        icon = 'cog fa-spin';
    } else if(type == 'error') {
        hide = ["info", "success"];
        element  = $(".error");
        icon    = 'exclamation';
    } else {
        hide = ["info", "error"];
        element = $(".success");
        icon = 'check';
    }

    if(title) {
        display += '<h5><i class="fa fa-' + icon + '"></i> ' + title + '</h5>';
    }

    if(typeof message === 'string' ) {
        display += '<p>' + message + '</p>';
    } else {
        display += '<ul>';
        $.each(message, function(i, item) {
            display += '<li>' + item + '</li>';
        });
        display += '</ul>';
    }

    $.each(hide, function(i, item) {
        $('.' + item).addClass('hidden');
    });

    element.html(display).removeClass('hidden');
}

function pad(val) { return val > 9 ? val : "0" + val; }
function startTimer(adapter)
{
    setInterval(function () {
        $("#" + adapter + "-seconds").html(pad(++window[adapter+"_uptime"] % 60));
        $("#" + adapter + "-minutes").html(pad(parseInt(window[adapter+"_uptime"] / 60, 10) % 60));
        $("#" + adapter + "-hours").html(pad(parseInt(window[adapter+"_uptime"] / 3600, 10)));
    }, 1000);
}

function dynamicSort(property) {
    var sortOrder = 1;
    if(property[0] === "-") {
        sortOrder = -1;
        property = property.substr(1);
    }
    return function (a,b) {
        var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
        return result * sortOrder;
    }
}