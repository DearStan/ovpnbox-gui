$(function () {

    /**
     * To open a port
     */
    $("#port_form").submit(function(event) {

        event.preventDefault();

        // Hide error message
        var error = $(".error"),
            device = $("#device"),
            port_number_1 = $("#port_number_1"),
            port_number_2 = $("#port_number_2"),
            type = $("#protocol"),
            button = $("#port_form").find('input[type=submit]'),
            table = $('.table'),
            info = $(".info"),
            portDisplay = false;

        error.addClass('hidden');

        displayMessage('info', $.i18n('port.opening'), $.i18n('port.working-on-open'));

        // Update button
        button.html('<i class="fa fa-circle-o-notch fa-spin"></i> ' + $.i18n('common.verifying'));
        button.prop("disabled",true);

        $.ajax({
            type: "POST",
            url:  "/port",
            data: {
                _token: $('input[name=_token]').val(),
                device: device.val(),
                port_number_1: port_number_1.val(),
                port_number_2: port_number_2.val(),
                protocol: type.val()
            },
            async: true,
            cache: false,
            timeout:120000,
            success: function () {

                if(port_number_1.val() == port_number_2.val()) {
                    portDisplay = port_number_1.val();
                } else {
                    portDisplay = port_number_1.val() + '-' + port_number_2.val();
                }

                $('#port-list').append(
                        '<tr>' +
                            '<td>' + device.find("option:selected").text() + '</td>' +
                            '<td>' + portDisplay + '</td>' +
                            '<td>' + type.find("option:selected").text() + '</td>' +
                            '<td><a href="javascript:void(0);" title="' + $.i18n('port.opened') + '"><i class="fa fa-check"></i></a></td></tr>');

                if(table.hasClass('hidden')) {
                    table.removeClass('hidden');
                    $(".ports-display").addClass('hidden');
                }

                info.addClass('hidden');
                button.prop("disabled",false);
                button.html($.i18n('port.open'));
                port_number_1.val('');
                port_number_2.val('');

            },
            error: function(xhr, textStatus, errorThrown ) {
                try {
                    var err = JSON.parse(xhr.responseText);
                } catch(error) {
                    var err = [{"error": [$.i18n('common.technical-error')]}];
                }

                displayMessage('error', $.i18n('common.error'), err);
                button.html($.i18n('port.open'));
                button.prop("disabled",false);
            }
        });


    });

    // Form that gets executed when user tries to delete a port forward
    $("body").on('click', '.delete_port', function() {

        // Hide error message
        var error = $(".error"), table = $('.table'), info = $(".info"), el = $(this);
        error.addClass('hidden');

        $.ajax({
            type: "DELETE",
            url:  "/port/" + el.data('portid'),
            data: {
              _token: $('input[name=_token]').val()
            },
            async: true,
            cache: false,
            timeout:120000,
            success: function () {

                el.parent().parent().remove();

                if($('tbody tr').length == 0) {
                    table.addClass('hidden');
                    $(".ports-display").removeClass('hidden');
                }

                info.addClass('hidden');
            },
            error: function(xhr, textStatus, errorThrown ) {
                try {
                    var err = JSON.parse(xhr.responseText);
                } catch(error) {
                    var err = [{"error": [$.i18n('common.technical-error')]}];
                }

                displayMessage('error', $.i18n('common.error'), err);
            }
        });
    });

});
