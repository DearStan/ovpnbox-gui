$(function () {

    var body = $("body"), deletion = 0;

    // Executed when user tries to remove a bypass
    body.on('click', '.deactivate_bypass', function() {

        displayMessage('info', $.i18n('devices.activating-protection'), $.i18n('devices.soon-protected'));
        var device = $(this).data('device');

        $.ajax({
            type: "DELETE",
            url:  "/devices/" + $(this).data('device') + "/bypass",
            data: {
                _token: $('.group-list').data('token')
            },
            async: true,
            cache: false,
            timeout:120000,
            success: function () {
                hideMessage();
                $("." + device + "-deactivate").addClass('hidden');
                $("." + device + "-activate").removeClass('hidden');
            },
            error: function(xhr, textStatus, errorThrown ) {
                try {
                    var err = JSON.parse(xhr.responseText);
                } catch(error) {
                    var err = [{"error": [$.i18n('common.technical-error')]}];
                }
                displayMessage('error', $.i18n('common.error'), err);
                return true;
            }
        });
    });

    // Executed when user tries to add a bypass
    body.on('click', '.activate_bypass', function() {

        displayMessage('info', $.i18n('devices.removing-protection'), $.i18n('devices.soon-not-protected'));
        var device = $(this).data('device');

        $.ajax({
            type: "POST",
            url:  "/devices/" + device + "/bypass",
            data: {
                _token: $('.group-list').data('token')
            },
            async: true,
            cache: false,
            timeout:120000,
            success: function () {
                hideMessage();
                $("." + device + "-activate").addClass('hidden');
                $("." + device + "-deactivate").removeClass('hidden');
            },
            error: function(xhr, textStatus, errorThrown ) {
                try {
                    var err = JSON.parse(xhr.responseText);
                } catch(error) {
                   var err = [{"error": [$.i18n('common.technical-error')]}];
                }
                displayMessage('error', $.i18n('common.error'), err);
                return true;
            }
        });
    });

    // Executed when user wants to change group for a device
    body.on('click', '.move_device', function() {

        var device = $(this).data('device'), name = $(this).data('name'), groups = $('.group-list').data('groups'),
            select = '', group = $(this).data('group');
        $('#modalTitle').text($.i18n('devices.where-move') + ' ' + name + '?');
        $('#device-move').val(device);
        select += '<option value="choose">' + $.i18n('devices.choose-group') + '</option>';

        $.each(groups, function(i, item) {
            if(item.id != group) {
                select += '<option value="' + item.id + '">' + item.name + '</option>';
            }
        });

        $('#groups-move').html(select);
        $('#deviceModal').modal();
    });

    body.on('submit', '.edit-group', function(e) {
        e.preventDefault();

        displayMessage('info', $.i18n('devices.updating'), $.i18n('devices.updating-name'));

        $.ajax({
            type: "PUT",
            url:  "/groups/" + $(this).data('group'),
            data: {
                name: $(this).find('input').val(),
                _token: $('.group-list').data('token')
            },
            async: true,
            cache: false,
            timeout:0,
            success: function () {
                displayMessage('info', $.i18n('devices.success'), $.i18n('devices.name-updated'));
                setTimeout(
                    function(){
                        window.location = '/devices';
                    },
                    1000
                );
            },
            error: function(xhr, textStatus, errorThrown ) {
                try {
                    var err = JSON.parse(xhr.responseText);
                } catch(error) {
                    var err = [{"error": [$.i18n('common.technical-error')]}];
                }
                displayMessage('error', $.i18n('common.error'), err);
                return true;
            }
        });
    });

    body.on('submit', '.edit-device-name', function(e) {
        e.preventDefault();

        var button = $(".device-rename-submit");
        button.prop("disabled",true);
        displayMessage('info', $.i18n('devices.updating'), $.i18n('devices.updating-device-name'));

        $.ajax({
            type: "PUT",
            url:  "/devices/" + $(this).data('device'),
            data: {
                name: $(this).find('input').val(),
                _token: $('.group-list').data('token')
            },
            async: true,
            cache: false,
            timeout:0,
            success: function () {
                displayMessage('info', $.i18n('devices.success'), $.i18n('devices.updated-device-name'));
                setTimeout(
                    function(){
                        window.location = '/devices';
                    },
                    1000
                );
            },
            error: function(xhr, textStatus, errorThrown ) {
                try {
                    var err = JSON.parse(xhr.responseText);
                } catch(error) {
                    var err = [{"error": [$.i18n('common.technical-error')]}];
                }
                displayMessage('error', $.i18n('common.error'), err);
                button.prop("disabled",false);
                return true;
            }
        });
    });

    // Move device to another group
    body.on('submit', '.move-device-form', function(e) {
        e.preventDefault();
        var button = $(this).find('.submit');
        // Update button
        button.html('<i class="fa fa-circle-o-notch fa-spin"></i> ' + $.i18n('common.verifying'));
        button.prop("disabled",true);

        $.ajax({
            type: "PUT",
            url:  "/devices/" + $('#device-move').val() + "/move",
            data: {
                group: $('#groups-move').val(),
                _token: $('.group-list').data('token')
            },
            async: true,
            cache: false,
            timeout:0,
            success: function () {
                $('.info-move').html($.i18n('devices.moved-device')).removeClass('hidden');
                setTimeout(
                    function(){
                        window.location = '/devices';
                    },
                    1000
                );
            },
            error: function(xhr, textStatus, errorThrown ) {
                try {
                    var err = JSON.parse(xhr.responseText);
                } catch(error) {
                    var err = [{"error": [$.i18n('common.technical-error')]}];
                }
                displayMessage('error', $.i18n('common.error'), err);
                button.html($.i18n('devices.move'));
                button.prop("disabled",false);
                return true;
            }
        });
    });

    // Executed when user wants to rename group
    body.on('click', '.rename_group', function() {
        showEditField($(this).data('group'));
    });

    // Executed when user wants to rename group
    body.on('click', '.rename_device', function() {
        showDeviceEditField($(this).data('device'));
    });

    // Executed when user wants to delete device from group
    body.on('click', '.delete_device', function() {

        if(deletion == 1) {
            displayMessage('info', $.i18n('devices.wait'), $.i18n('devices.cant-remove-more'));
            return false;
        }

        deletion = 1;

        displayMessage('info', $.i18n('devices.removing'), $.i18n('devices.removing-device'));
        var error = $(".error"), table = $('.table'), id = $(this).data('device');
        error.addClass('hidden');

        $.ajax({
            type: "DELETE",
            url:  "/devices/" + id,
            data: {
                _token: $('.group-list').data('token')
            },
            async: true,
            cache: false,
            timeout:120000,
            success: function () {
                deletion = 0;
                displayMessage('info', false, $.i18n('devices.renew-dhcp'));
                removeDeviceFromGroup(id)
            },
            error: function(xhr, textStatus, errorThrown ) {
                deletion = 0;
                try {
                    var err = JSON.parse(xhr.responseText);
                } catch(error) {
                    var err = [{"error": [$.i18n('common.technical-error')]}];
                }

                displayMessage('error', $.i18n('common.error'), err);
            }
        });
    });

    // Executed when user wants to delete device from group
    body.on('click', '.delete_group', function() {

        if(deletion == 1) {
            displayMessage('info', $.i18n('devices.wait'), $.i18n('devices.cant-remove-group-more'));
            return false;
        }

        deletion = 1;

        hideMessage();
        displayMessage('info', $.i18n('devices.removing'), $.i18n('devices.removing-device'));

        $.ajax({
            type: "DELETE",
            url:  "/groups/" + $(this).data('group'),
            data: {
                _token: $('.group-list').data('token')
            },
            async: true,
            cache: false,
            timeout:120000,
            success: function () {
                deletion = 0;
                displayMessage('info', $.i18n('devices.success'), $.i18n('devices.removed-group'));
                setTimeout(
                    function(){
                        window.location = '/devices';
                    },
                    2000
                );
            },
            error: function(xhr, textStatus, errorThrown ) {
                deletion = 0;
                try {
                    var err = JSON.parse(xhr.responseText);
                } catch(error) {
                    var err = [{"error": [$.i18n('common.technical-error')]}];
                }

                displayMessage('error', $.i18n('common.error'), err);
            }
        });
    });

    /**
     * Adds device to a group
     */
    $(".devicegroup").submit(function(event) {
        var id = $(this).data('id');
        event.preventDefault();

        // Hide error message
        var error = $(".error"),
            info = $(".info"),

            name = $('#device'+id).val(),
            mac = $('#mac'+id).val(),
            group = $('#group'+id).val(),

            token = $('#token'+id).val(),
            button = $('#submit'+id);

        error.addClass('hidden');

        displayMessage('info', $.i18n('devices.adding'), $.i18n('devices.grouping-device'));

        // Update button
        button.attr('value', $.i18n('common.verifying'));
        button.prop("disabled",true);

        $.ajax({
            type: "post",
            url:  "/devices",
            data: {
                _token: token,
                name: name,
                mac: mac,
                group: group
            },
            async: true,
            cache: false,
            timeout:120000,
            success: function (output) {

                if(!$('.group-'+output.group.id).length) {
                    addGroup(output.group.id, output.group.title);
                }


                addDeviceToGroup(
                    output.group.id,
                    output.id,
                    output.device,
                    output.ip,
                    output.mac
                );

                removeDeviceFromUnsorted(id);
                displayMessage('info', false, $.i18n('devices.renew-dhcp'));
            },
            error: function(xhr, textStatus, errorThrown ) {
                try {
                    var err = JSON.parse(xhr.responseText);
                } catch(error) {
                    var err = [{"error": [$.i18n('common.technical-error')]}];
                }

                info.addClass('hidden');
                displayMessage('error', $.i18n('common.error'), err);
                button.attr('value', $.i18n('devices.save'));
                button.prop("disabled",false);
            }
        });
    });
});

function addGroup(id, name) {
    $(".group-list").append(
        '<div class="row"><div class="col-sm-12"><div class="panel panel-default">' +
        '<div class="panel-heading" style="min-height: 60px;"><div class="row"><div class="col-sm-8">' +

        '<span id="group-' + id + '"><span id="group-' + id + '-container" class="group-title">' + name + '</span><br /></span>' +
        '<span class="label label-danger">NEDKOPPLAD</span></div><div class="col-sm-4 text-right" style="margin-top: 13px;">' +
        '<a href="#" class="rename_group" data-group="' + id + '"><i class="fa fa-pencil-square-o"></i></a></div></div></div>' +
        '<div class="panel-body"><table class="table table-hover group-' + id + '" data-count="0">' +
            '<thead>' +
                '<tr>' +
                    '<th>#</th>' +
                    '<th>' + $.i18n('devices.name') + '</th>' +
                    '<th>' + $.i18n('devices.ip') + '</th>' +
                    '<th>' + $.i18n('devices.mac') + '</th>' +
                    '<th>' + $.i18n('devices.status') + '</th>' +
                    '<th>' + $.i18n('devices.handle') + '</th>' +
                '</tr>' +
            '</thead>' +
            '<tbody>' +
            '</tbody>' +
        '</table>'
    );
}

function checkConnectivity(device_id)
{
    var el = $("#connectivity-" + device_id);

    $.ajax({
        type: "GET",
        url:  "/devices/" + device_id + "/status",
        data: {
            _token: $('.group-list').data('token')
        },
        async: true,
        cache: false,
        timeout:0,
        success: function (output) {
            if(output.status) {
                el.text('Ansluten');
            } else {
                el.text('-');
            }
        },
        error: function() {
            el.text('Fel');
        }
    });
}

function showEditField(group_id) {
    $("#group-" + group_id).html(
        '<form class="form-inline edit-group" data-group="' + group_id + '">' +
            '<div class="form-group">' +
                '<input type="text" class="form-control" value="' + $("#group-" + group_id + "-container").text() + '">' +
            '</div>&nbsp;&nbsp;' +
            '<button type="submit" class="btn btn-default">' + $.i18n('devices.save') + '</button>' +
        '</form>');
}

function showDeviceEditField(device_id) {
    var container =  $("#device-" + device_id + "-container");
    container.html(
        '<form class="form-inline edit-device-name" data-device="' + device_id + '">' +
        '<div class="form-group">' +
        '<input type="text" class="form-control" value="' + container.text() + '">' +
        '</div>&nbsp;&nbsp;' +
        '<button type="submit" class="btn btn-default device-rename-submit">' + $.i18n('devices.save') + '</button>' +
        '</form>');
}

function addDeviceToGroup(group_id, device_id, name, ip, mac) {
    var group = $(".group-" + group_id);
    var count = group.data('count')+1;

    // Add to table
    group.find('tbody').append(
    '<tr class="alert alert-success device-' + device_id + '">' +
        '<th scope="row">' + count + '</th>' +
        '<td>' + name + '</td>' +
        '<td>' + ip + '</td>' +
        '<td>' + mac + '</td>' +
        '<td>-</td>' +
        '<td></td>' +
    '</tr>');
    group.data('count', count);
    group.removeClass('hidden');
    $(".no-devices-" + group_id).addClass('hidden');
}

function removeDeviceFromGroup(id) {
    $(".device-" + id).addClass('hidden');
}

function removeDeviceFromUnsorted(id) {
    $(".unsorted-" + id).addClass('hidden');

    var dynamic = $('.dynamicips');
    var count = dynamic.data('count');

    if(count == 0) {
        dynamic.addClass('hidden');
        $('.dynamicips-none').removeClass('hidden');
        dynamic.data('count', (count-1))
    }
}
