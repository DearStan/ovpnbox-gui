$(function () {

    // Fetch data regarding the current state
    var holder = $("#dataholder"), serverSelect = $('#server_ip'),
        connectionType = $('#connection_type'),
        addonSelect = $("#addonId"),show_connection = $("#show_connection"),
        hide_connection = $("#hide_connection");

    // Event listener for changing connection type
    connectionType.change(function() {
        if($(this).val() == 'manual') {
            serverSelect.parent().removeClass('hidden');
        } else {
            serverSelect.parent().addClass('hidden');
        }
    });

    show_connection.click(function(event) {
        $(".form_connection").removeClass("hidden");
        show_connection.addClass('hidden');
        hide_connection.removeClass("hidden");
    });

    hide_connection.click(function(event) {
        $(".form_connection").addClass("hidden");
        show_connection.removeClass('hidden');
        hide_connection.addClass("hidden");
    });

    $('.show_log').click(function(event) {

        var group = $(this).data('group'), adapter = $(this).data('adapter');

        $.ajax({
            type: "GET",
            url:  "/connection/" + group,
            async: true,
            timeout:60000,
            success: function (output) {
                $('#' + adapter + '-openvpn-body').html(output);
                $('#' + adapter + '-openvpn').modal();
            },
            error: function(xhr, textStatus, errorThrown ) {

                try {
                    var err = JSON.parse(xhr.responseText);
                } catch(error) {
                    var err = [{"error": [$.i18n('common.technical-error')]}];
                }

                displayMessage('error', $.i18n('common.error'), err);
                return true;

            }
        });
        return true;
    });

    $('.disconnect').click(function(event) {

        var button = $(this);
        button.prop("disabled",true);
        $.ajax({
            type: "DELETE",
            url:  "/connection/" + button.data('group'),
            data: {
                _token: $(this).data('token')
            },
            async: true,
            timeout:0,
            success: function (output) {
                displayMessage('info', $.i18n('devices.success'), $.i18n('dashboard.devices-disconnected'));
                setTimeout(
                    function(){
                        window.location = '/';
                    },
                    3000
                );
            },
            error: function(xhr, textStatus, errorThrown ) {

                try {
                    var err = JSON.parse(xhr.responseText);
                } catch(error) {
                    var err = [{"error": [$.i18n('common.technical-error')]}];
                }
                button.prop("disabled",false);
                displayMessage('error', $.i18n('common.error'), err);
                return true;

            }
        });
        return true;
    });

    $('.reconnect').click(function(event) {

        var button = $(this);
        button.prop("disabled",true);
        $.ajax({
            type: "PUT",
            url:  "/connection/" + button.data('group'),
            data: {
                _token: $(this).data('token')
            },
            async: true,
            timeout:0,
            success: function (output) {
                displayMessage('info', $.i18n('devices.success'), $.i18n('dashboard.devices-reconnected'));
                setTimeout(
                    function(){
                        window.location = '/';
                    },
                    3000
                );
            },
            error: function(xhr, textStatus, errorThrown ) {

                try {
                    var err = JSON.parse(xhr.responseText);
                } catch(error) {
                    var err = [{"error": [$.i18n('common.technical-error')]}];
                }
                button.prop("disabled",false);
                displayMessage('error', $.i18n('common.error'), err);
                return true;

            }
        });
        return true;
    });


    /**
     * Används när man ska ansluta. Antingen har personen valt 'Välj bäst server' eller så
     * har personen valt en specifik server.
     */
    $("#connect").submit(function(event) {
        event.preventDefault();

        // Hämta värdet i selectmenyn
        var ip = $("#server_ip").val(), type = $("#connection_type").val(),
            group = $("#group").val(), killswitch = $("#killswitch").val(),
            addon = $("#addon").val();
        connect(type, ip, group, addon, killswitch);

        return false;
    });

});

function connect(type, ip, group, addon, killswitch) {

    $('.submit').prop("disabled", true);

    $.ajax({
        type: "POST",
        url:  "/",
        data: {
            type: type,
            ip: ip,
            addon: addon,
            killswitch: killswitch,
            group: group,
            _token: $('input[name=_token]').val()
        },
        async: true,
        timeout:0,
        success: function () {
            setTimeout(
                function(){
                    window.location = '/';
                },
                1000
            );
        },
        error: function(xhr, textStatus, errorThrown ) {

            try {
                var err = JSON.parse(xhr.responseText);
            } catch(error) {
                var err = [{"error": [$.i18n('common.technical-error')]}];
            }

            displayMessage('error', $.i18n('common.error'), err);
            $('.submit').prop("disabled", false);
            return true;

        }
    });
    return true;
}