$(function () {

    var body = $("body");

    // Executed when user tries to remove a bypass
    body.on('click', '.reload_settings', function() {

        var reload = $('.fa-refresh');
        reload.addClass('fa-spin');
        displayMessage('info', $.i18n('common.fetching'), $.i18n('ovpn.fetching-account'));

        $.ajax({
            type: "PUT",
            url:  "/ovpn",
            data: {
                _token: $(this).data('token')
            },
            async: true,
            cache: false,
            timeout:0,
            success: function () {
                reload.removeClass('fa-spin');
                displayMessage('success', $.i18n('common.updated'), $.i18n('ovpn.updated-account'));
                setTimeout(
                    function(){
                        window.location = '/ovpn';
                    },
                    3000
                );
            },
            error: function(xhr, textStatus, errorThrown ) {
                reload.removeClass('fa-spin');

                try {
                    var err = JSON.parse(xhr.responseText);
                } catch(error) {
                    var err = [{"error": [$.i18n('common.technical-error')]}];
                }

                displayMessage('error', $.i18n('common.error'), err);
                return true;
            }
        });
    });
});