$(function () {

    $("#username").focus();

    /**
     * Används när man ska ansluta. Antingen har personen valt 'Välj bäst server' eller så
     * har personen valt en specifik server.
     */
    $("#login").submit(function(event) {
        event.preventDefault();

        // Hide error message
        var error = $(".error"), username = $("#username"), password = $("#password"), remember = $("#remember"), button = $("#login").find('button'), rememberme = 'off';
        error.addClass('hidden');

        // Update button
        button.html('<i class="fa fa-circle-o-notch fa-spin"></i> ' + $.i18n('common.verifying'));

        if(remember.prop('checked')) {
            rememberme = "on";
        }

        $.ajax({
            type: "POST",
            url:  "/login",
            data: {
                _token: $('input[name=_token]').val(),
                name: username.val(),
                password: password.val(),
                remember: rememberme
            },
            async: true,
            cache: false,
            timeout:120000,
            success: function () {
                window.location = '/';
            },
            error: function(xhr, textStatus, errorThrown ) {
                try {
                    var err = JSON.parse(xhr.responseText);
                } catch(error) {
                    var err = [{"error": [$.i18n('common.technical-error')]}];
                }

                displayMessage('error', $.i18n('common.error'), err);
                button.html($.i18n('common.sign-in'));
            }
        });
    });

});
