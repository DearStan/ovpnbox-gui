$(function () {
    $("#reset_form").submit(function(event) {
        event.preventDefault();

        var email = $('#email').val(), button = $('button[type=submit]'),
            token = $('input[name=_token]').val();

        hideMessage();
        displayMessage('info', 'Verifying credentials', 'Verifying credentials and attempting to send a password reset link.');

        // Update button
        button.html($.i18n('common.verifying'));
        button.prop("disabled",true);

        $.ajax({
            type: "POST",
            url:  "/password/email",
            data: {
                _token: token,
                email: email
            },
            async: true,
            cache: false,
            timeout:120000,
            success: function (output) {
                displayMessage('success', 'Email sent', 'A password reset email has been sent to the specified email address.');
                return true;
            },
            error: function(xhr, textStatus, errorThrown ) {
                try {
                    var err = JSON.parse(xhr.responseText);
                } catch(error) {
                    var err = [{"error": [$.i18n('common.technical-error')]}];
                }

                displayMessage('error', $.i18n('common.error'), err);
                button.html('Send Password Reset Link');
                button.prop("disabled",false);
            }
        });
        return true;
    });
});