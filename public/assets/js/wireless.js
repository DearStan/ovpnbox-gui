$(function () {

    // Event listener for changing connection type
    $("#wireless").change(function() {
        if($(this).val() == 'enable') {
            $(".activated-option").removeClass('hidden');
        } else {
            $(".activated-option").addClass('hidden');
        }
    });

    $("#network_form").submit(function (event) {

        event.preventDefault();

        // Hide error message
        var error = $(".error"),
            wireless = $("#wireless"),
            network = $("#network"),
            frequency = $("#frequency"),
            password = $("#password"),
            button = $("#network_form").find('button'),
            password_confirmation = $('#password_confirmation'),
            info = $(".info");

        error.addClass('hidden');

        displayMessage('info', $.i18n('wireless.saving'), $.i18n('wireless.working-saving'));

        // Update button
        button.html('<i class="fa fa-circle-o-notch fa-spin"></i> ' + $.i18n('common.verifying'));
        button.prop("disabled", true);

        $.ajax({
            type: "POST",
            url: "/wireless",
            data: {
                _token: $('input[name=_token]').val(),
                wireless: wireless.val(),
                network: network.val(),
                frequency: frequency.val(),
                password: password.val(),
                password_confirmation: password_confirmation.val()
            },
            async: true,
            cache: false,
            timeout: 120000,
            success: function () {

                displayMessage('success', $.i18n('devices.success'), $.i18n('wireless.changes-saved'));
                button.prop("disabled", false);
                button.html($.i18n('wireless.save-changes'));

            },
            error: function (xhr, textStatus, errorThrown) {
                button.html($.i18n('wireless.save-changes'));
                button.prop("disabled", false);
            }
        });

    });
});