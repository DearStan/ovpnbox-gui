$(function () {

    $("#admin").submit(function (event) {

        event.preventDefault();

        // Hide error message
        var button = $('button[type=submit]');

        hideMessage();
        displayMessage('info', $.i18n('settings.updating'), $.i18n('settings.updating-text'));

        // Update button
        button.html('<i class="fa fa-circle-o-notch fa-spin"></i> ' + $.i18n('common.verifying'));
        button.prop("disabled", true);

        $.ajax({
            type: "PUT",
            url: "/settings/admin",
            data: {
                _token: $('input[name=_token]').val(),
                name: $('input[name=name]').val(),
                email: $('input[name=email]').val(),
                language: $('select[name=language]').val(),
                password: $('input[name=password]').val(),
                password_confirmation: $('input[name=password_confirmation]').val()
            },
            async: true,
            cache: false,
            timeout: 120000,
            success: function () {
                displayMessage('success', $.i18n('devices.success'), $.i18n('settings.admin-successful'));
                button.prop("disabled", false);
                button.html($.i18n('settings.update'));
            },
            error: function (xhr, textStatus, errorThrown) {
                try {
                    var err = JSON.parse(xhr.responseText);
                } catch(error) {
                    var err = [{"error": [$.i18n('common.technical-error')]}];
                }
                displayMessage('error', $.i18n('common.error'), err);

                button.html($.i18n('settings.update'));
                button.prop("disabled", false);
            }
        });

    });

    $("#ovpn").submit(function (event) {

        event.preventDefault();

        // Hide error message
        var button = $('button[type=submit]');

        hideMessage();
        displayMessage('info', $.i18n('settings.verifying'), $.i18n('settings.verifying-text'));

        // Update button
        button.html('<i class="fa fa-circle-o-notch fa-spin"></i> ' + $.i18n('common.verifying'));
        button.prop("disabled", true);

        $.ajax({
            type: "PUT",
            url: "/settings/ovpn",
            data: {
                _token: $('input[name=_token]').val(),
                username: $('input[name=ovpn_name]').val(),
                password: $('input[name=ovpn_password]').val()
            },
            async: true,
            cache: false,
            timeout: 120000,
            success: function () {
                displayMessage('success', $.i18n('devices.success'), $.i18n('settings.ovpn-updated'));
                button.prop("disabled", false);
                button.html($.i18n('settings.update'));
            },
            error: function (xhr, textStatus, errorThrown) {
                try {
                    var err = JSON.parse(xhr.responseText);
                } catch(error) {
                    var err = [{"error": [$.i18n('common.technical-error')]}];
                }
                displayMessage('error', $.i18n('common.error'), err);

                button.html($.i18n('settings.update'));
                button.prop("disabled", false);
            }
        });

    });
});