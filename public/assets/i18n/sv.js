var LOCALE_SV = {
    "@metadata": {
        "authors": [
            "David Wibergh"
        ],
        "last-updated": "2016-04-25",
        "locale": "sv"
    },

    // common phrases
    "common.technical-error": "Ett tekniskt fel har skett.",
    "common.starting-configuration": "Påbörjar konfiguration...",
    "common.start-configuration": "Påbörja konfiguration",
    "common.error": "Fel",
    "common.signing-in": "Inloggning pågår",
    "common.verifying-credentials": "OVPNbox arbetar på att verifiera inloggningsuppgifterna & ändra inställningar i boxen.",
    "common.verifying": "Verifierar",
    "common.sign-in": "Logga in",
    "common.updated": "Uppdaterat",
    "common.fetching": "Hämtar",
    "common.create-account": "Skapa konto",
    "common.creating-account": "Skapar konto",
    "common.verifying-and-creating": "OVPNbox verifierar uppgifterna och försöker skapa ett administratörskonto.",
    "common.status": "Status",


    // device phrases
    "devices.activating-protection": "Aktiverar skydd",
    "devices.soon-protected": "Enheten kommer inom kort att återigen skyddas av OVPN.",
    "devices.removing-protection": "Inaktiverar skydd",
    "devices.soon-not-protected": "Skyddet för enheten inaktiveras nu. Enhetens anslutningen kommer inte att anonymiseras.",
    "devices.where-move": "Vart vill du flytta",
    "devices.choose-group": "Välj grupp",
    "devices.updating-name": "Uppdaterar namnet på gruppen.",
    "devices.updating": "Uppdaterar",
    "devices.success": "Lyckades!",
    "devices.name-updated": "Namnet på gruppen har uppdaterats. Gränssnittet kommer snart att laddas om.",
    "devices.updating-device-name": "Uppdaterar namnet på enheten.",
    "devices.updated-device-name": "Namnet på enheten har uppdaterats. Gränssnittet kommer snart att laddas om.",
    "devices.moved-device": "Enheten har flyttats till den nya gruppen. Gränssnittet kommer snart att laddas om.",
    "devices.move": "Flytta",
    "devices.wait": "Vänta",
    "devices.cant-remove-more": "Du kan inte ta bort en enhet medans en annan hanteras.",
    "devices.removing": "Tar bort",
    "devices.removing-device": "Enheten håller på att tas bort från gruppen.",
    "devices.renew-dhcp": "För att enheten ska få den nya IP-adressen måste den startas om, alternativt måste DHCP-inställningarna laddas om.",
    "devices.grouping-device": "OVPNbox arbetar på att gruppera enheten.",
    "devices.adding": "Lägger till",
    "devices.save": "Spara",
    "devices.name": "Namn",
    "devices.ip": "IP",
    "devices.mac": "MAC-adress",
    "devices.status": "Status",
    "devices.handle": "Hantera",
    "devices.cant-remove-group-more": "Du kan inte ta bort en grupp medans en annan hanteras.",
    "devices.removing-group": "Gruppen och alla enheter i den håller på att bli borttagna.",
    "devices.removed-group": "Gruppen och alla enheter har nu blivit borttagna. Gränssnittet kommer snart att laddas om.",


    // dashboard phrases
    "dashboard.devices-disconnected": "Enheterna i gruppen har kopplats ner. Gränssnittet kommer snart att laddas om.",
    "dashboard.devices-reconnected": "Enheterna i gruppen har återanslutits. Gränssnittet kommer snart att laddas om.",
    "dashboard.connecting": "Ansluter",
    "dashboard.disconnecting": "Kopplar från",


    // ovpn phrases
    "ovpn.fetching-account": "Hämtar information från OVPN.",
    "ovpn.updated-account": "Din kontoinformation har uppdaters. Gränssnittet laddas snart om.",


    // port phrases
    "port.opening": "Öppnar port",
    "port.working-on-open": "OVPNbox arbetar på att vidarebefordra porten till enheten.",
    "port.opened": "Porten har vidarebefordrats!",
    "port.open": "Öppna port",


    // statistics phrases
    "stats.downloaded": "Nedladdat",
    "stats.uploaded": "Uppladdat",


    // system phrases
    "system.reboot": "OVPNbox startar om. Det kan ta upp till fem minuter innan den är tillgänglig igen.",
    "system.rebooting": "Startar om",
    "system.poweroff": "Stänger av",
    "system.poweroff-working": "OVPNbox stängs av. Du måste manuellt starta upp den igen genom att trycka på strömknappen på OVPNbox.",


    // wireless phrases
    "wireless.saving": "Sparar inställningar",
    "wireless.working-saving": "OVPNbox arbetar på att spara ändringarna som genomförts.",
    "wireless.changes-saved": "Ändringarna har sparats och applicerats.",
    "wireless.save-changes": "Spara inställningar",


    // settings phrases
    "settings.admin-successful": "Administratörskontot har uppdaterats med den nya information.",
    "settings.update": "Uppdatera",
    "settings.updating": "Uppdaterar",
    "settings.updating-text": "Uppdaterar ditt administratörskonto.",
    "settings.verifying": "Verifierar",
    "settings.verifying-text": "Verifierar de angivna uppgifterna med OVPN.",
    "settings.ovpn-updated": "OVPN-kontot som användas för boxen har uppdaterats till det angivna."
};