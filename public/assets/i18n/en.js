var LOCALE_EN = {
    "@metadata": {
        "authors": [
            "David Wibergh"
        ],
        "last-updated": "2016-04-15",
        "locale": "en"
    },

    // common phrases
    "common.technical-error": "A technical error has occurred.",
    "common.starting-configuration": "Initializing the configuration...",
    "common.start-configuration": "Start configuration",
    "common.error": "Error",
    "common.signing-in": "Signing in.",
    "common.verifying-credentials": "OVPNbox is verifying the entered credentials and changing settings.",
    "common.verifying": "Verifying",
    "common.sign-in": "Sign in",
    "common.updated": "Updated",
    "common.fetching": "Fetching",
    "common.create-account": "Create account",
    "common.creating-account": "Creating account",
    "common.verifying-and-creating": "OVPNbox is verifying the credentials and attempting to create the admin account.",
    "common.status": "Status",


    // device phrases
    "devices.activating-protection": "Activating protection",
    "devices.soon-protected": "The device will soon once again be protected by OVPN.",
    "devices.removing-protection": "Deactivating protection",
    "devices.soon-not-protected": "The device\'s protection is being deactivated. The device will use your ISPs connection going forward.",
    "devices.where-move": "Where do you want to move",
    "devices.choose-group": "Choose group",
    "devices.updating-name": "Updating the group\'s name.",
    "devices.updating": "Updating",
    "devices.success": "Success!",
    "devices.name-updated": "The group\'s name has been updated. The page will reload shortly.",
    "devices.updating-device-name": "Updating the name for the device.",
    "devices.updated-device-name": "The device name has been updated. The page will reload shortly.",
    "devices.moved-device": "The device has been moved to the new group. The page will reload shortly.",
    "devices.move": "Move",
    "devices.wait": "Wait",
    "devices.cant-remove-more": "You can\'t remove a device while another one is being handled.",
    "devices.removing": "Removing",
    "devices.removing-device": "The device is being removed from the group.",
    "devices.renew-dhcp": "In order for the device to get the static IP address assigned, it will either need to be restarted or to renew the DHCP settings on it.",
    "devices.grouping-device": "OVPNbox is adding the device to the specified group.",
    "devices.adding": "Adding",
    "devices.save": "Save",
    "devices.name": "Name",
    "devices.ip": "IP",
    "devices.mac": "MAC address",
    "devices.status": "Status",
    "devices.handle": "Handle",
    "devices.cant-remove-group-more": "You can\'t remove a group while another one is being handled.",
    "devices.removing-group": "The group and all devices in it are being removed.",
    "devices.removed-group": "The group and all the devices in the group have been removed. The page will reload shortly.",


    // dashboard phrases
    "dashboard.devices-disconnected": "The devices in the group have been disconnected. The page will reload shortly.",
    "dashboard.devices-reconnected": "The devices in the group have been reconnected. The page will reload shortly.",
    "dashboard.connecting": "Connecting",
    "dashboard.disconnecting": "Disconnecting",


    // ovpn phrases
    "ovpn.fetching-account": "Fetching account information from OVPN.",
    "ovpn.updated-account": "Your account information has been updated. The page will reload shortly.",


    // port phrases
    "port.opening": "Opening port",
    "port.working-on-open": "OVPNbox is working on forwarding the port to the device.",
    "port.opened": "The port has been forwarded!",
    "port.open": "Open port",


    // statistics phrases
    "stats.downloaded": "Downloaded",
    "stats.uploaded": "Uploaded",


    // system phrases
    "system.reboot": "OVPNbox is rebooting. It can take up to five minutes before it's available again.",
    "system.rebooting": "Rebooting",
    "system.poweroff": "Powering off",
    "system.poweroff-working": "OVPNbox is currently powering off. You will need to manually boot it again next time by clicking on the power button on the OVPNbox.",


    // wireless phrases
    "wireless.saving": "Saving settings",
    "wireless.working-saving": "OVPNbox is working on applying the settings.",
    "wireless.changes-saved": "The settings have been saved and applied.",
    "wireless.save-changes": "Save settings",


    // settings phrases
    "settings.admin-successful": "The administrator account has been updated with the new credentials.",
    "settings.update": "Update",
    "settings.updating": "Updating",
    "settings.updating-text": "Updating your administrator account details.",
    "settings.verifying": "Verifying",
    "settings.verifying-text": "Verifying your account credentials with OVPN.",
    "settings.ovpn-updated": "The OVPN account on OVPNbox has been updated.",
};