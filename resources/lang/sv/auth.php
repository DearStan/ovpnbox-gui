<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Felaktiga inloggningsuppgifter',
    'throttle' => 'För många misslyckade inloggningsförsök. Vänligen vänta :seconds sekunder innan du försöker på nytt.',
    'login_to_access' => 'För att komma åt gränssnittet på OVPNbox måste du logga in på kontot du skapade.',
    'supply_credentials' => 'Ange ditt användarnamn och lösenord.',
    'username' => 'Användarnamn',
    'password' => 'Lösenord',
    'remember_me' => 'Kom ihåg mig',
    'sign_in' => 'Logga in',
    'sign_out' => 'Logga ut',
    'reset' => 'Återställ lösenord',
    '' => '',
    '' => '',
    '' => '',


];
