<?php
return [

    /*
    |--------------------------------------------------------------------------
    | System Language Lines
    |--------------------------------------------------------------------------
    */

    'network_adapters' => 'Nätverksadaptrar',
    'web_server' => 'Webbserver',
    'hardware' => 'Hårdvara',
    'box_uptime' => 'OVPNbox upptid',
    'load' => 'Belastning',
    'network_cards' => 'Nätverkskort',
    'processor' => 'Processor',
    'max_ram' => 'Max RAM',
    'ram_module' => 'RAM modul',
    'temperature' => 'Temperatur',
    'reboot' => 'Starta om OVPNbox',
    'poweroff' => 'Stäng av OVPNbox',
    'handle' => 'Hantera',
    'new_version' => 'Ny version finns tillgänglig',
    'no_update_available' => 'Ingen uppdatering tillgänglig',
    'update_in_pfsense' => 'Uppdatera i pfSense',
    'gui' => 'Gränsnitt',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',

];
