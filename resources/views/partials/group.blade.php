<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading" style="min-height: 60px;">
                <div class="row">
                    <div class="col-sm-8">
                        <span class="group-title">{{ $group->name }}</span><br />
                        @if($group->adapter->connected)
                            @if(!is_null($group->connection()->active()->first()))
                                {{ $group->connection()->active()->first()->directives['event']['server'] }},
                                {{ $group->connection()->active()->first()->directives['event']['ip_info']['ip'] }},
                                {{ $group->connection()->active()->first()->directives['event']['ip_info']['ptr'] }}<br />
                                {{ $group->connection()->active()->first()->directives['event']['addon'] }},
                                <span id="{{ $group->adapter->name }}-hours">hh</span>:<span id="{{ $group->adapter->name }}-minutes">mm</span>:<span id="{{ $group->adapter->name }}-seconds">ss</span>
                            @else
                                {{ trans('dashboard.potential_bug') }}
                            @endif
                        @else
                            <span class="label label-danger">{{ trans('dashboard.disconnected') }}</span>
                        @endif
                    </div>
                    <div class="col-sm-4 text-right" style="margin-top: 13px;">
                        <button type="button" class="btn btn-default btn-sm reconnect" data-group="{{ $group->id }}" data-token="{{ csrf_token() }}">
                            {{ trans('dashboard.reconnect') }}
                        </button>&nbsp;
                        <button type="button" class="btn btn-default btn-sm disconnect" data-group="{{ $group->id }}" data-token="{{ csrf_token() }}">
                            {{ trans('dashboard.disconnect') }}</button>
                        &nbsp;
                        <button type="button" class="btn btn-default btn-sm show_log" data-group="{{ $group->id }}" data-adapter="{{ $group->adapter->name }}" data-token="{{ csrf_token() }}">
                            {{ trans('dashboard.show_log') }}
                        </button>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                @if($group->adapter->connected)
                    <div class="text-center">
                        <span style="color:#1EB300;margin-left: 10px;"><i class="fa fa-arrow-down"></i>&nbsp;<span id="{{ $group->adapter->name }}-input">-</span> Mbit/s</span>&nbsp;
                        <span style="color:#6D88AD"><i class="fa fa-arrow-up"></i>&nbsp;<span id="{{ $group->adapter->name }}-output">-</span> Mbit/s</span>
                    </div>
                    <div id="{{ $group->adapter->name }}" class="hidden-xs" style="min-width: 400px; height: 300px; margin: 0 auto"></div>
                @else
                    <h4 class="text-center" style="margin-bottom: 0px;">{{ trans('dashboard.connection_lost') }}</h4>
                    @if($group->connection()->active()->first()->directives['input']['killswitch'] == "active")
                        <p><strong>{{ trans('dashboard.calm_ks_activated') }}</strong></p>
                    @else
                        <p>{{ trans('dashboard.devices_normal_connection') }}</p>
                    @endif
                    <p>
                        {!! trans('dashboard.reconnect_or_disconnect') !!}
                    </p>
                @endif
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="{{ $group->adapter->name }}-openvpn" tabindex="-1" role="dialog" aria-labelledby="{{ $group->adapter->name }}-openvpn">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body" id="{{ $group->adapter->name }}-openvpn-body">

            </div>
        </div>
    </div>
</div>