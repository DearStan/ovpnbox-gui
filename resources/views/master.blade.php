<!DOCTYPE html>
<html @if(Auth::check()) lang="{{ Auth::user()->language }}" @else lang="en" @endif dir="ltr">
<head>
    <meta charset="UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title>@if(isset($meta['title'])) {{ $meta['title'] }} @else Sida @endif | OVPNbox</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}"/>

    <!-- stylesheets -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/theme.css') }}?v={{ md5_file(public_path() . '/assets/css/theme.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/main.css') }}?v={{ md5_file(public_path() . '/assets/css/main.css') }}">

    <!-- javascript -->
    <script src="{{ asset('assets/js/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/js/functions.js') }}?v={{ md5_file(public_path() . '/assets/js/functions.js') }}"></script>
    <script src="{{ asset('assets/js/i18n/libs/CLDRPluralRuleParser/src/CLDRPluralRuleParser.js') }}"></script>
    <script src="{{ asset('assets/js/i18n/src/jquery.i18n.js') }}"></script>
    <script src="{{ asset('assets/js/i18n/src/jquery.i18n.messagestore.js') }}"></script>
    <script src="{{ asset('assets/js/i18n/src/jquery.i18n.parser.js') }}"></script>
    <script src="{{ asset('assets/js/i18n/src/jquery.i18n.language.js') }}"></script>
    <script src="{{ asset('assets/i18n/en.js') }}?v={{ md5_file(public_path() . '/assets/i18n/en.js')}}"></script>
    <script src="{{ asset('assets/i18n/sv.js') }}?v={{ md5_file(public_path() . '/assets/i18n/sv.js')}}"></script>

    @if(isset($meta['page']))

        @if($meta['page'] == 'setup')
            @if(file_exists(public_path() . '/assets/js/setup/step' . Request::segment(2) . '.js'))
                <script src="{{ asset('/assets/js/setup/step' . Request::segment(2) . '.js') }}?v={{ md5_file(public_path() . '/assets/js/setup/step' . Request::segment(2) . '.js') }}"></script>
            @elseif(count(Request::segments()) == 1)
                <script src="{{ asset('/assets/js/setup/step1.js') }}?v={{ md5_file(public_path() . '/assets/js/setup/step1.js') }}"></script>
            @endif
        @endif

        @if(file_exists(public_path() . '/assets/js/' . $meta['page'] . '.js'))
            <script src="{{ asset('/assets/js/' . $meta['page'] . '.js') }}?v={{ md5_file(public_path() . '/assets/js/' . $meta['page'] . '.js') }}"></script>
        @endif

        @if($meta['page'] == 'index' || $meta['page'] == 'stats')
            <script src="{{ asset('/assets/js/highstock.js') }}"></script>
        @endif

    @else
        @if(Request::segment(1) == 'password')
            <script src="{{ asset('assets/js/password.js') }}?v={{ md5_file(public_path() . '/assets/js/password.js')}}"></script>
        @endif
    @endif

</head>

<body id="signup">
<div class="container">
    <div class="header">
        <h3 class="logo">
            <a href="@if(Request::segment(1) == 'setup')#@else/@endif" title="OVPN.se">
                <img src="{{ asset('assets/img/ovpn-logo.png') }}" alt="OVPN.se" title="OVPN.se"></a>
        </h3>
    </div>
    <div class="wrapper clearfix">
        <div class="formy">
            @if(isset($meta['page']) && !in_array($meta['page'], ['setup', 'login']))
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                    data-target="#bs-navbar-collapse-1">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse text-center" id="bs-navbar-collapse-1">
                            <ul class="nav navbar-nav">
                                <li @if($meta['page'] == 'index') class="active" @endif>
                                    <a href="/">{{ trans('general.connection') }}</a>
                                </li>
                                <li @if($meta['page'] == 'stats') class="active" @endif>
                                    <a href="/stats">{{ trans('general.statistics') }}</a></li>
                                <li @if($meta['page'] == 'devices') class="active" @endif>
                                    <a href="/devices">{{ trans('general.devices') }}</a>
                                </li>
                                <li @if($meta['page'] == 'port') class="active" @endif>
                                    <a href="/port">{{ trans('general.ports') }}</a>
                                </li>
                                @if(App\Services\Shell\System::getWifiCard())
                                    <li @if($meta['page'] == 'wireless') class="active" @endif>
                                        <a href="/wireless">{{ trans('general.wireless') }}</a>
                                    </li>
                                @endif
                                <li @if($meta['page'] == 'ovpn') class="active" @endif>
                                    <a href="/ovpn">OVPN</a>
                                </li>
                                <li class="dropdown @if(in_array($meta['page'], ['settings', 'system'])) active @endif">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ trans('general.more') }} <span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li @if($meta['page'] == 'settings') class="active" @endif>
                                            <a href="/settings">{{ trans('general.settings') }}</a>
                                        </li>
                                        <li @if($meta['page'] == 'system') class="active" @endif>
                                            <a href="/system">{{ trans('general.system') }}</a>
                                        </li>

                                    </ul>
                                </li>

                            </ul>
                        </div>
                        <!-- /.navbar-collapse -->
                    </div>
                    <!-- /.container-fluid -->
                </nav>
            @endif
            <div class="error alert alert-danger hidden"></div>
            <div class="success alert alert-success hidden"></div>
            <div class="info alert alert-info hidden"></div>
            <br>

            @yield('content')

        </div>
    </div>
    <div class="already-account">
        @if(Auth::user()) <a href="/logout/">{{ trans('auth.sign_out') }}</a> @else <a href="/password/email">{{ trans('auth.reset') }}</a> @endif
    </div>
</div>
<div id="dataholder"></div>
<script src="{{ asset('assets/js/socket.io/socket.io.min.js') }}"></script>
<script>
    var socket = io('http://{{ App\Services\Shell\System::getLanIp() }}:3000'), conversion = 0.000007629;

    $(function () {

        @if(Auth::check())
            localize("{{ Auth::user()->language }}");
        @else
            localize('en');
        @endif


        socket.on('ovpn:connect', function(data) {
            displayMessage('info', $.i18n("dashboard.connecting"), data.message);
        });

        socket.on('ovpn:disconnect', function(data) {
            displayMessage('info', $.i18n("dashboard.disconnecting"), data.message);
        });

        socket.on('ovpn:wireless', function(data) {
            displayMessage('info', $.i18n('devices.updating'), data.message);
        });

        socket.on('ovpn:devices', function(data) {
            displayMessage('info', $.i18n('common.status'), data.message);
        });

        @if(isset($meta['page']) && $meta['page'] == 'index')

        socket.on('ovpn:TrafficStatistics', function(data) {

            if(window[data.interface].series[0] !== undefined) {
                var adapter = window[data.interface], input = adapter.series[0], output = adapter.series[1],
                        shift_input = input.data.length > 100, shift_output = output.data.length > 100,
                        down_bytes = data.in_bytes, up_bytes = data.out_bytes, dataholder = $("#dataholder");

                if (typeof dataholder.data(data.interface + '-timestamp') != 'undefined') {
                    previousDownBytes = dataholder.data(data.interface + '-down');
                    previousUpBytes = dataholder.data(data.interface + '-up');
                    previousTimestamp = dataholder.data(data.interface + '-timestamp');
                } else {
                    previousDownBytes = down_bytes;
                    previousUpBytes = up_bytes;
                    previousTimestamp = data.timestamp;
                    window[data.interface + "_uptime"] = data.uptime;
                    startTimer(data.interface);
                }

                var diffDownBytes = (down_bytes - previousDownBytes), diffUpBytes = (up_bytes - previousUpBytes),
                        diffTimestamp = (data.timestamp - previousTimestamp);

                if (diffTimestamp <= 0) {
                    diffTimestamp = 1;
                }

                downSpeed = parseFloat(((diffDownBytes * conversion) / diffTimestamp).toFixed(2));
                upSpeed = parseFloat(((diffUpBytes * conversion) / diffTimestamp).toFixed(2));

                dataholder.data(data.interface + '-down', down_bytes);
                dataholder.data(data.interface + '-up', up_bytes);
                dataholder.data(data.interface + '-timestamp', data.timestamp);

                var x = (new Date((data.timestamp * 1000))).getTime();

                input.addPoint([x, downSpeed], true, shift_input);
                output.addPoint([x, (upSpeed * -1)], true, shift_output);

                // Display numeric values
                $("#" + data.interface + "-input").text(downSpeed);
                $("#" + data.interface + "-output").text(upSpeed);
            }
        });

        @elseif(isset($meta['page']) && ($meta['page'] == 'devices' || $meta['page'] == 'stats'))

            socket.on('ovpn:TrafficStatistics', function(data) {

                var dataholder = $("#dataholder");

                if (typeof dataholder.data(data.interface + '-timestamp') == 'undefined') {
                    window[data.interface+"_uptime"] = data.uptime;
                    dataholder.data(data.interface + '-timestamp', data.timestamp);
                    startTimer(data.interface);
                }
            });

        @endif
    });
</script>
</body>
</html>