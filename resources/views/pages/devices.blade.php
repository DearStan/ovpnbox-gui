@extends('master')

@section('content')

<div class="group-list" data-token="{{ csrf_token() }}" data-groups='{!! json_encode($groups->toArray()) !!}'>
    @if(!$groups->isEmpty())
        @foreach($groups as $group)
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-default">
                        <div class="panel-heading" style="min-height: 60px;">
                            <div class="row">
                                <div class="col-sm-8">
                                    <span id="group-{{ $group->id }}"><span id="group-{{ $group->id }}-container" class="group-title">{{ $group->name }}</span><br /></span>
                                    @if($group->adapter['connected'])
                                        @if(!is_null($group->connection()->active()->first()))
                                            {{ $group->connection()->active()->first()->directives['event']['server'] }},
                                            {{ $group->connection()->active()->first()->directives['event']['addon'] }},
                                            <span id="{{ $group->adapter->name }}-hours">hh</span>:<span id="{{ $group->adapter->name }}-minutes">mm</span>:<span id="{{ $group->adapter->name }}-seconds">ss</span>,  
                                            {{ $group->traffic_download }} GB /   {{ $group->traffic_upload }} GB
                                            <script>var {{ $group->adapter->name }}, {{ $group->adapter->name }}_uptime = 0;</script>
                                        @else
                                            {{ trans('devices.potential_bug') }}
                                        @endif
                                    @else
                                        <span class="label label-danger">{{ trans('devices.disconnected') }}</span>
                                    @endif
                                </div>
                                <div class="col-sm-4 text-right" style="margin-top: 13px;">
                                    <a href="#" class="rename_group" data-group="{{ $group->id }}">
                                        <i class="fa fa-pencil-square-o"></i>
                                    </a>
                                    @if(!$group->adapter['connected'])
                                        <a href="#" class="delete_group" data-group="{{ $group->id }}">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="alert alert-info no-devices-{{ $group->id }} @if(!$group->devices->isEmpty()) hidden @endif" role="alert" style="margin-bottom: 0;">{{ trans('devices.no_devices_in_group') }}</div>
                            <table class="table table-hover group-{{ $group->id }} @if($group->devices->isEmpty()) hidden @endif" data-count="{{ $group->devices->count() }}">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{{ trans('devices.name') }}</th>
                                        <th>{{ trans('devices.ip') }}</th>
                                        <th>{{ trans('devices.mac_address') }}</th>
                                        <th>{{ trans('devices.status') }}</th>
                                        <th>{{ trans('devices.handle') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @if(!$group->devices->isEmpty())
                                    @foreach($group->devices as $x => $device)
                                        <tr class="device-{{ $device->id }}">
                                            <th scope="row">{{ ($x+1) }}</th>
                                            <td>
                                                <span id="device-{{ $device->id }}-container">{{ $device->name }}</span>
                                            </td>
                                            <td>{{ $device->ip }}</td>
                                            <td>{{ $device->mac }}</td>
                                            <td id="connectivity-{{ $device->id }}"><i class="fa fa-cog fa-spin"></i></td>
                                            <td>
                                                <a href="javascript:void(0);" class="rename_device" title="{{ trans('devices.rename_device') }}"
                                                   data-device="{{ $device->id }}" data-name="{{ $device->name }}"><i class="fa fa-pencil"></i></a>
                                                <a href="javascript:void(0);" class="deactivate_bypass {{ $device->id }}-deactivate @if(!$device->bypass) hidden @endif"
                                                   title="{{ trans('devices.activate_protection') }}" data-device="{{ $device->id }}"><i class="fa fa-unlock"></i></a>
                                                <a href="javascript:void(0);" class="activate_bypass {{ $device->id }}-activate @if($device->bypass) hidden @endif"
                                                   title="{{ trans('devices.deactivate_protection') }}" data-device="{{ $device->id }}"><i class="fa fa-lock"></i></a>
                                                <a href="javascript:void(0);" class="move_device" title="{{ trans('devices.move_device') }}"
                                                   data-device="{{ $device->id }}" data-name="{{ $device->name }}" data-group="{{ $group->id }}"><i class="fa fa-group"></i></a>
                                                <a href="javascript:void(0);" class="delete_device" title="{{ trans('devices.delete_device') }}"
                                                   data-device="{{ $device->id }}"><i class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    @endif
</div>

<h4>{{ trans('devices.unsorted_devices') }}
    @if(!is_null($hidden))
        <br />
        <span style="color:#333;font-size:13px;">  
            @if($hidden->adapter->connected)
                {{ $hidden->connection()->active()->first()->directives['event']['server'] }},
                {{ $hidden->connection()->active()->first()->directives['event']['addon'] }},
                <span id="{{ $hidden->adapter->name }}-hours">hh</span>:<span id="{{ $hidden->adapter->name }}-minutes">mm</span>:<span id="{{ $hidden->adapter->name }}-seconds">ss</span>,  
                {{ $hidden->traffic_download }} GB /   {{ $hidden->traffic_upload }} GB
            @else
                <span class="label label-danger">{{ trans('devices.disconnected') }}</span>
            @endif
                          </span>
        <script>var {{ $hidden->adapter->name }}, {{ $hidden->adapter->name }}_uptime = 0;</script>
    @endif
</h4>

<div class="alert alert-info @if(!empty($unsorted)) hidden @endif dynamicips-none" role="alert">{{ trans('devices.all_devices_sorted') }}</div>
<table class="@if(empty($unsorted)) hidden @endif dynamicips table table-hover" data-count="{{ count($unsorted) }}">
    <thead>
    <tr>
        <th>#</th>
        <th width="100">{{ trans('devices.name') }}</th>
        <th width="100">{{ trans('devices.ip') }}</th>
        <th width="100">{{ trans('devices.mac_address') }}</th>
        <th>{{ trans('devices.handle') }}</th>
    </tr>
    </thead>
    <tbody>
        @if(!empty($unsorted))
            @foreach($unsorted as $x => $device)
                <form method="post" data-id="{{ $x }}" class="form-inline devicegroup">
                    <tr class="unsorted-{{ $x }}">
                        <th scope="row">{{ ($x+1) }}</th>
                        <td width="100">
                            <div class="form-group">
                                <input type="text" class="form-control" name="device" id="device{{ $x }}" value="@if(isset($device['hostname'])){{ $device['hostname'] }}@else {{ trans('devices.unknown') }} @endif">
                            </div>
                        </td>
                        <td width="100">{{ $device['ip'] }}</td>
                        <td width="100">@if(isset($device['mac'])) {{ $device['mac'] }} @else - @endif</td>
                        <td>
                            <div class="form-group">
                                <select name="group" id="group{{ $x }}" class="form-control">
                                    @if(!$groups->isEmpty())
                                        <option value="choose">{{ trans('devices.choose_group') }}</option>
                                        @foreach($groups as $group)
                                            <option value="{{ $group->id }}">{{ $group->name }}</option>
                                        @endforeach
                                    @endif
                                        <option value="create">{{ trans('devices.create_group') }}</option>
                                </select>
                            </div>
                        </td>
                        <td>
                            <div class="form-group">
                                <input type="hidden" name="mac" id="mac{{ $x }}" value="{{ $device['mac'] }}" />
                                <input type="hidden" name="token" id="token{{ $x }}" value="{{ csrf_token() }}" />
                                <input type="submit" class="btn btn-primary" id="submit{{ $x }}" value="{{ trans('devices.save') }}" />
                            </div>
                        </td>
                    </tr>
                </form>
            @endforeach
        @endif
    </tbody>
</table>


<!-- Modal -->
<div class="modal fade" id="deviceModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modalTitle"></h4>
            </div>
            <div class="modal-body" id="modalBody">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="info-move alert alert-info hidden"></div>
                        <form class="move-device-form form-horizontal">
                            <div class="form-group">
                                <input type="hidden" id="device-move" value="">
                                <select id="groups-move" class="form-control">

                                </select>
                            </div>
                            <div class="form-group">
                                <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('devices.cancel') }}</button>
                                <button type="submit" class="btn btn-primary submit">{{ trans('devices.move') }}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(function () {
        @foreach($devices as $device)
            checkConnectivity({{ $device->id }});
        @endforeach
    });
</script>
@endsection