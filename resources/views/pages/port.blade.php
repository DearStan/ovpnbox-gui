@extends('master')

@section('content')

    <h4>{{ trans('port.portforwarding') }}</h4>

    <div class="alert alert-info ports-display @if(!$ports->isEmpty()) hidden @endif" role="alert">{{ trans('port.no_portforwards') }}
    </div>
    <table class="table table-hover @if($ports->isEmpty()) hidden @endif">
        <thead>
        <tr>
            <th>{{ trans('port.device') }}</th>
            <th>{{ trans('port.port_range') }}</th>
            <th>{{ trans('port.type') }}</th>
            <th>{{ trans('port.handle') }}</th>
        </tr>
        </thead>
        <tbody id="port-list">
        @if(!$ports->isEmpty())
            @foreach($ports as $port)
                <tr id="port-{{ $port->id }}">
                    <td>{{ $port->device->name }}
                        <small>({{ $port->device->ip }})</small>
                    </td>
                    <td>{{ $port->range }}</td>
                    <td>{{ $port->type }}</td>
                    <td>
                        <a href="javascript:void(0);" class="delete_port" title="{{ trans('port.remove_port_forward') }}"
                           data-portid="{{ $port->id }}"><i class="fa fa-trash"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
    <br/><br/>
    <h4>{{ trans('port.new_portforward') }}</h4>
    @if($devices->isEmpty())
        <div class="alert alert-info" role="alert">{!! trans('port.require_static_ip') !!}</div>
    @else
        <form role="form" class="form-inline" id="port_form" method="post">
            {{ csrf_field() }}
            <div class="form-group">
                <select class="form-control" name="device" id="device">
                    @foreach($devices as $device)
                        <option value="{{ $device->id }}">{{ $device->name }} - {{ $device->ip }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <div class="input-group" style="width:180px;">
                    <input type="text" class="form-control" id="port_number_1" name="port_number_1" placeholder="Port"/>
                    <div class="input-group-addon">-</div>
                    <input type="text" class="form-control" id="port_number_2" name="port_number_2" placeholder="Port"/>
                </div>
            </div>
            <div class="form-group">
                <select class="form-control" name="protocol" id="protocol">
                    <option value="1">TCP</option>
                    <option value="2">UDP</option>
                    <option value="3">TCP/UDP</option>
                </select>
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="{{ trans('port.open_port') }}" />
            </div>
        </form>
    @endif
    <br/><br/>
    <h4>OVPN</h4>
    @if(is_null($account->ports))
        <div class="alert alert-info" role="alert">
            @if(is_null($account->public_ipv4))
                {{ trans('port.no_open_ports') }}
            @else
                {{ trans('port.no_open_ports_public_ipv4') }}
            @endif
        </div>
    @else
        <table class="table table-hover">
            <thead>
            <tr>
                <th>{{ trans('port.port') }}</th>
                <th>{{ trans('port.type') }}</th>
            </tr>
            </thead>
            <tbody>
                @foreach($account->ports as $port)
                    <tr>
                        <td>
                            {{ $port['port'] }}
                        </td>
                        <td>
                            @if($port['type'] == 'both')
                                TCP/UDP
                            @else
                                {{ strtoupper($port['type']) }}
                            @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @endif
@endsection