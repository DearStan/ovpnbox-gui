@extends('master')

@section('content')

    <h4>{{ trans('system.handle') }}</h4>
    <div class="row">
        <div class="col-sm-6">
            <div class="alert alert-cp alert-info wireless">
                <div class="row">
                    <div class="col-sm-6 col-xs-6">
                        <div class="status-block">
                            <form role="form" method="post" action="#" id="reboot" style="margin-top:15px;">
                                <div class="form-group">
                                    {{ csrf_field() }}
                                    <button type="submit" name="submit" class="btn btn-default btn-sm">{{ trans('system.reboot') }}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-6">
                        <div>
                            <form role="form" method="post" action="#" id="poweroff" style="margin-top:15px;">
                                <div class="form-group">
                                    {{ csrf_field() }}
                                    <button type="submit" name="submit" class="btn btn-default btn-sm">{{ trans('system.poweroff') }}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="alert alert-cp alert-info wireless" style="min-height: 72px;">
                <div class="row">
                    <div class="col-sm-6 col-xs-6">
                        <div class="status-block">
                            <h5>OVPNbox</h5>
                            <small>@if($version['firmware']) {{ trans('system.new_version') }}<br />{{ trans('system.update_in_pfsense') }} @else {{ trans('system.no_update_available') }} @endif</small>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-6">
                        <div>
                            <h5>{{ trans('system.gui') }}</h5>
                            <small>@if($version['gui']) {{ trans('system.new_version') }} @else {{ trans('system.no_update_available') }} @endif</small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><br /><br />

    <h4>{{ trans('system.network_adapters') }}</h4>
    <table class="table table-hover">
        <tbody>
        @foreach($interfaces as $interface)
            <tr>
                <td width="200">{{ strtoupper($interface->type) }} - <small>{{ $interface->name }} </small></td>
                <td>
                    @if($interface->type != 'wireless')
                        {{ $traffic[$interface->name]['download'] }} GB / {{ $traffic[$interface->name]['upload'] }} GB
                    @else
                        -
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <h4>OVPNbox</h4>
    <table class="table table-hover table-width-325">
        <tbody>
        <tr>
            <td width="200">pfSense</td>
            <td>{{ $system['pfsense'] }}</td>
        </tr>
        <tr>
            <td>FreeBSD</td>
            <td>{{ $system['freebsd'] }}</td>
        </tr>
        <tr>
            <td>Box</td>
            <td><a href="{{ $system['version']['link'] }}" target="_blank">{{ $system['version']['short'] }}/{{ $system['version']['branch'] }}</a></td>
        </tr>
        <tr>
            <td>PHP</td>
            <td>{{ $system['php'] }}</td>
        </tr>
        <tr>
            <td>{{ trans('system.web_server') }}</td>
            <td>{{ $system['web'] }}</td>
        </tr>
        </tbody>
    </table>

    <h4>{{ trans('system.hardware') }}</h4>
    <table class="table table-hover">
        <tbody>
        <tr>
            <td width="200">{{ trans('system.box_uptime') }}</td>
            <td><span id="system-hours">hh</span>:<span id="system-minutes">mm</span>:<span id="system-seconds">ss</span></td>
        </tr>
        <tr>
            <td>{{ trans('system.load') }}</td>
            <td>{{ $system['hardware']['load'] }}</td>
        </tr>
        <tr>
            <td>{{ trans('system.processor') }}</td>
            <td>{{ $system['hardware']['processor'] }}</td>
        </tr>
        @foreach($system['hardware']['nics'] as $interface => $nic)
            <tr>
                <td>{{ trans('system.network_cards') }} ({{ $interface }})</td>
                <td>{{ $nic }}</td>
            </tr>
        @endforeach
        <tr>
            <td>{{ trans('system.max_ram') }}</td>
            <td>{{ $system['hardware']['ram']['max'] }}</td>
        </tr>
        @foreach($system['hardware']['ram']['modules'] as $key => $ram)
            <tr>
                <td>{{ trans('system.ram_module') }} {{ ($key+1) }}</td>
                <td>{{ $ram['manufacturer'] }} {{ $ram['size'] }} {{ $ram['part'] }} {{ $ram['speed'] }}</td>
            </tr>
        @endforeach
        <tr>
            <td>{{ trans('system.temperature') }}</td>
            <td>{{ $system['hardware']['temperature'] }}&#176;C</td>
        </tr>
        </tbody>
    </table>

    <script>
        var system_uptime = {{ $system['hardware']['uptime'] }};

        $(function () {
            startTimer('system');
        });
    </script>
@endsection