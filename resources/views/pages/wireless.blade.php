@extends('master')

@section('content')

    @if($enabled)
        <h4>{{ trans('wireless.wireless') }}</h4>
        <div class="row">
            <div class="col-sm-6">
                <div class="alert alert-cp alert-info wireless">
                    <div class="row">
                        <div class="col-sm-6 col-xs-6">
                            <div class="status-block">
                                <h5>SSID</h5>
                                <small>{{ $wifi->ssid }}</small>
                            </div>
                        </div>
                        <div class="col-sm-6 col-xs-6">
                            <div>
                                <h5>{{ trans('wireless.frequency') }}</h5>
                                <small>802.{{ $wifi->standard }}, {{ trans('wireless.channel') }} {{ $wifi->channel }}</small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="alert alert-cp alert-info wireless">
                    <div class="row">
                        <div class="col-sm-6 col-xs-6">
                            <div class="status-block">
                                <h5>DHCP</h5>
                                <small>{{ $range }}</small>
                            </div>
                        </div>
                        <div class="col-sm-6 col-xs-6">
                            <div>
                                <h5>{{ trans('wireless.security') }}</h5>
                                <small>WPA{{ $wifi->wpa->wpa_mode }} {{ $wifi->wpa->wpa_key_mgmt }}</small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><br /><br />
    @endif

    <h4>{{ trans('wireless.settings') }}</h4>
    <div class="col-sm-6">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form method="post" id="network_form">
            <div class="form-group">
                <label for="frequency">{{ trans('wireless.wireless_network') }}</label>
                <select name="wireless" id="wireless" class="form-control">
                    <option value="enable" @if($enabled) selected @endif>{{ trans('wireless.activated') }}</option>
                    <option value="disable" @if(!$enabled) selected @endif>{{ trans('wireless.deactivated') }}</option>
                </select>
            </div>
            <div class="activated-option @if(!$enabled) hidden @endif">
                <div class="form-group">
                    <label for="network">{{ trans('wireless.network_name') }}</label>
                    <input type="text" class="form-control" id="network" name="network" value="@if($wifi){{ $wifi->ssid }}@endif" />
                </div>
                <div class="form-group">
                    <label for="frequency">{{ trans('wireless.frequency') }}</label>
                    <select name="frequency" id="frequency" class="form-control">
                        <option value="2" @if($wifi && $wifi->channel == 1) selected @endif>2.4 Ghz (150 Mbit/s)</option>
                        <option value="5" @if($wifi && $wifi->channel == 64) selected @endif>5 Ghz (300 Mbit/s)</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="password">{{ trans('wireless.password') }}</label>
                    <input type="password" class="form-control" id="password" name="password" />
                </div>
                <div class="form-group">
                    <label for="password2">{{ trans('wireless.repeat_password') }}</label>
                    <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" />
                </div>
            </div>
            <div class="form-group">
                {{ csrf_field() }}
                <button type="submit" name="submit" class="btn btn-primary">{{ trans('wireless.save_settings') }}</button>
            </div>
        </form>
    </div>
    <div class="col-sm-12">
        <div class="alert alert-info">{{ trans('wireless.information') }}</div>
    </div>



@endsection