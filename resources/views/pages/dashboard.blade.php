@extends('master')

@section('content')

<div class="row @if($connected->isEmpty()) hidden @endif">
    <div class="col-sm-12">
        <div class="pull-right">
            <button type="button" id="show_connection" class="btn btn-primary"><i class="fa fa-plus"></i> {{ trans('dashboard.new_connection') }}</button>
            <button type="button" id="hide_connection" class="btn btn-primary hidden"><i class="fa fa-minus"></i> {{ trans('dashboard.hide') }}</button>
            <br /><br />
        </div>
    </div>
</div>
<div class="row form_connection @if(!$connected->isEmpty()) hidden @endif">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title" style="color: #3B88B6;font-weight: 400;">{{ trans('dashboard.new_connection') }}</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    @if($disconnected->isEmpty())
                        <div class="col-sm-12">
                            <div class="alert alert-info" role="alert">{{ trans('dashboard.all_groups_connected') }}</div>
                        </div>
                    @else
                        <div class="col-sm-6">
                            <div class="choose-server">
                                <form role="form" method="post" action="#" id="connect">
                                    <div class="form-group">
                                        <select class="form-control" id="group">
                                            @foreach($disconnected as $group)
                                                <option value="{{ $group->id }}">{{ $group->name }} @if(!$group->hidden) - {{ $group->devices->count() }} @if($group->devices->count() == 1) {{ trans('dashboard.device') }} @else {{ trans('dashboard.devices') }} @endif @endif</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <select class="form-control" id="connection_type">
                                            <option value="auto">{{ trans('dashboard.choose_best_server_auto') }}</option>
                                            <option value="DE">{{ trans('dashboard.choose_best_server_de') }}</option>
                                            <option value="SE">{{ trans('dashboard.choose_best_server_se') }}</option>
                                            <option value="manual">{{ trans('dashboard.choose_manual') }}</option>
                                        </select>
                                    </div>
                                    <div class="form-group hidden">
                                        <select class="form-control" id="server_ip">
                                            @if($servers)
                                                @foreach($servers as $server)
                                                    <option value="{{ $server->ip }}">{{ $server->name }}</option>
                                                @endforeach
                                            @else
                                                <option value="0">{{ trans('dashboard.techinical_error') }}</option>
                                            @endif
                                        </select>
                                    </div>
                                    <!-- Denna select meny visas och är populerad med tilläggstjänsterna som är aktiva. Om inga tilläggstjänster finns är den dold. -->
                                    <div class="choose-addon form-group @if(!$ovpn->addons) hidden @endif">
                                        <select class="form-control" id="addon">
                                            <option value="normal" selected>{{ trans('dashboard.no_addon') }}</option>
                                            @if($ovpn->addons)
                                                @foreach($ovpn->addons as $addon) {
                                                <option value="{{ $addon }}">{{ \App\Services\Helpers\String::addonAbbreviationToText($addon) }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <select class="form-control" id="killswitch">
                                            <option value="active">{{ trans('dashboard.activate_killswitch') }}</option>
                                            <option value="inactive">{{ trans('dashboard.inactivate_killswitch') }}</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" name="submit" class="btn btn-primary submit">{{ trans('dashboard.connect') }}</button>
                                    </div>
                                    {{ csrf_field() }}
                                </form>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

@if(!$connected->isEmpty())
    @foreach($connected as $group)
        @include('partials.group', ['group' => $group])
        <script>
            var {{ $group->adapter->name }}, {{ $group->adapter->name }}_uptime = 0;
            $(document).ready(function() {

                Highcharts.setOptions({
                    global : {
                        timezoneOffset: -120
                    }
                });

                {{ $group->adapter->name }} = new Highcharts.Chart({
                    chart: {
                        defaultSeriesType: 'areaspline',
                        renderTo: '{{ $group->adapter->name }}'
                    },
                    tooltip: {
                        formatter: function () {
                            var s = '<b>' + new Date(this.x).toLocaleString() + '</b>', val = 0;

                            $.each(this.points, function () {
                                if(this.y < 0) {
                                    val = (this.y*-1);
                                } else {
                                    val = this.y;
                                }

                                s += '<br/><span style="color:' + this.series.color + '">•</span> ' + this.series.name + ': ' +
                                        val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ');
                            });

                            return s;
                        },
                        shared: true
                    },
                    rangeSelector: {
                        enabled: false
                    },
                    navigator: {
                        enabled: false
                    },
                    scrollbar: {
                        enabled: false
                    },
                    title: {
                        text: null
                    },
                    yAxis: {
                        title: {
                            text: null
                        }
                    },
                    xAxis: {
                        type: 'datetime'
                    },
                    series : [{
                        name : '{{ trans('dashboard.download_speed') }}',
                        color: '#1EB300',
                        fillOpacity: 0.2,
                        data : (function () {
                            var data = [], time = (new Date()).getTime(), i;
                            var dataLength = 299;

                            // display speed values for each 2 second
                            for (i = -1*dataLength; i <= 0; i += 1) {
                                data.push([
                                    time + i * 2000,
                                    0
                                ]);
                            }
                            return data;
                        }())
                    },{
                        name : '{{ trans('dashboard.upload_speed') }}',
                        color: '#6D88AD',
                        fillOpacity: 0.2,
                        data : (function () {
                            var data = [], time = (new Date()).getTime(), i;
                            var dataLength = 299;

                            // display speed values for each 2 second
                            for (i = -1*dataLength; i <= 0; i += 1) {
                                data.push([
                                    time + i * 2000,
                                    0
                                ]);
                            }
                            return data;
                        }())
                    }
                    ]
                });
            });
        </script>
    @endforeach
@endif
@endsection