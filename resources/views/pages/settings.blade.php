@extends('master')

@section('content')

    <div class="col-sm-6">
        <h4>{{ trans('settings.admin_account') }}</h4>
        <form method="post" id="admin">
            {!! csrf_field() !!}
            <div class="form-group">
                <label for="name">{{ trans('settings.username') }}</label>
                <input type="text" class="form-control" id="name" name="name" value="{{ $user->name }}" />
            </div>
            <div class="form-group">
                <label for="email">{{ trans('settings.email') }}</label>
                <input type="email" class="form-control" id="email" name="email" value="{{ $user->email }}" />
            </div>
            <div class="form-group">
                <label for="password">{{ trans('settings.password') }}</label>
                <input type="password" class="form-control" id="password" name="password" />
            </div>
            <div class="form-group">
                <label for="password_confirmation">{{ trans('settings.repeat') }}</label>
                <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" />
            </div>
            <div class="form-group">
                <label for="language">{{ trans('settings.language') }}</label>
                <select class="form-control" id="language" name="language">
                    <option value="en" @if($user->language == "en") selected @endif>English</option>
                    <option value="sv" @if($user->language == "sv") selected @endif>Swedish</option>
                </select>
            </div>
            <div class="form-group text-center">
                {{ csrf_field() }}
                <button type="submit" name="submit" class="btn btn-primary">{{ trans('settings.update') }}</button>
            </div>
        </form>
    </div>

    <div class="col-sm-6">
        <h4>{{ trans('settings.ovpn_account') }}</h4>
        <form method="post" id="ovpn">
            {!! csrf_field() !!}
            <div class="form-group">
                <label for="ovpn_name">{{ trans('settings.username') }}</label>
                <input type="text" class="form-control" id="ovpn_name" name="ovpn_name" value="{{ $ovpn->username }}" />
            </div>
            <div class="form-group">
                <label for="ovpn_password">{{ trans('settings.password') }}</label>
                <input type="password" class="form-control" id="ovpn_password" name="ovpn_password" />
            </div>
            <div class="form-group text-center">
                {{ csrf_field() }}
                <button type="submit" name="submit" class="btn btn-primary">{{ trans('settings.update') }}</button>
            </div>
        </form>
    </div>
@endsection